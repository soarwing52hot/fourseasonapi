from typing import List


class LegacyRouter:
    """
    A router to control all database operations on models in the
    legacy application.
    legacy database name: 4seasondb
    all app with legacy label should route to 4seasondb
    """
    router_app_label: List[str] = ["legacy"]

    def db_for_read(self, model, **hints):
        """
        Attempts to read legacy models go to legacy.
        """

        if model._meta.app_label in self.router_app_label:
            return "legacy"
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write legacy models go to legacy.
        """
        if model._meta.app_label in self.router_app_label:
            return "legacy"
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the legacy app is involved.
        """
        if obj1._meta.model_name in self.router_app_label or obj2._meta.model_name in self.router_app_label:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the legacy app only appears in the 'legacy'
        database.
        """
        if app_label in self.router_app_label:
            return db == "legacy"
        return None
