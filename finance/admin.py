from django.contrib import admin

from .models import Payment


class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "email",
        "type",
        "account_number",
        "transfer_date",
        "memo",
    )


admin.site.register(Payment, PaymentAdmin)
