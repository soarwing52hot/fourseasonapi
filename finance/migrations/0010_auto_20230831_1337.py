# Generated by Django 3.2.9 on 2023-08-31 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finance', '0009_auto_20230714_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='modify_date',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='payment',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
