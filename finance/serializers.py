from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Payment

USER = get_user_model()


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = "__all__"

    # id = serializers.IntegerField(required=False)
    # email = serializers.EmailField(required=False)
    # name = serializers.CharField(max_length=20, required=False)
    # type = serializers.CharField(max_length=20, required=False)
    # phone = serializers.CharField(max_length=10, required=False)
    # account_number = serializers.CharField(max_length=5, required=False)
    # transfer_date = serializers.DateField(required=False)
    # memo = serializers.CharField(required=False)
    # confirmed = serializers.BooleanField(required=False)
    # amount = serializers.IntegerField(required=False)


class PaymentConfirmationSerializer(serializers.Serializer):
    class Meta:
        fields = "__all__"

    confirmed = serializers.BooleanField(default=False)
    confirm_person = serializers.CharField()

    def update(self, instance, validated_data):
        instance.confirmed = validated_data.get("confirmed", instance.confirm_person)
        instance.confirm_person = USER.objects.get(membership_number=int(validated_data.get("confirm_person")))
        instance.save()
        return instance
