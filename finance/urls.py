from django.urls import include, path
from rest_framework.routers import DefaultRouter

from finance import views

from .views import InvoiceViewSet, PaymentConfirmViewSet, PaymentViewSet

router = DefaultRouter()
router.register(r"Payment", PaymentViewSet)
router.register(r"PaymentConfirm", PaymentConfirmViewSet)
router.register(r"Invoice", InvoiceViewSet, basename="Invoice")

payment_list = PaymentViewSet.as_view({"get": "list"})

payment_send = InvoiceViewSet.as_view({"get": "send_invoice"})

urlpatterns = [
    path("", include(router.urls)),
    path("Invoice/<int:pk>/send/", payment_send, name="send invoice mail"),
    path("types/", views.PaymentTypeOptionsAPIView.as_view(), name="camp_types"),
]
