import json
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from cryptography.fernet import Fernet
from django.shortcuts import get_object_or_404
from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from common.serializers import OptionSelectSerialzer
from finance import models, serializers
from finance.models import Payment
from finance.serializers import PaymentConfirmationSerializer, PaymentSerializer


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.all().order_by("-creation_date")
    serializer_class = PaymentSerializer
    permission_classes = [permissions.AllowAny]
    http_method_names = ["get", "post"]

    def perform_create(self, serializer):
        if self.request.user.is_anonymous:
            serializer.save()
            return
        serializer.save(member=self.request.user)


class PaymentConfirmViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentConfirmationSerializer
    permission_classes = [permissions.AllowAny]
    http_method_names = ["put"]


class PaymentTypeOptionsAPIView(APIView):
    def get(self, request):
        types = models.PaymentTypes.choices
        serializer = OptionSelectSerialzer(types, many=True)
        return Response(serializer.data)


# TODO Use Serializer to parse encode id
class InvoiceViewSet(viewsets.ViewSet):
    basename = "invoice"
    key = os.environ.get("INVOICE_KEY")

    def list(self, request):
        pass

    def retrieve(self, request, pk=None):
        """
        根據帳號末五碼、交易日期、收據編號進行篩選
        """
        rq_message = Fernet(self.key).decrypt(pk).decode()
        rq_dict = json.loads(rq_message)
        account_number = rq_dict["account_number"]
        transfer_date = rq_dict["transfer_date"]
        id = rq_dict["id"]
        queryset = Payment.objects.all()
        payment = get_object_or_404(queryset, account_number=account_number, id=id, transfer_date=transfer_date)
        serializer = PaymentSerializer(payment)
        return Response(serializer.data)

    def send_invoice(self, request, pk=None):
        def send_mail(src, dest, subject, content):
            content = MIMEText(content, "html")
            content["Subject"] = subject
            content["From"] = "四季財務組"
            content["To"] = dest
            # content["CC"] = 'secretary@4season.org.tw'

            with smtplib.SMTP(host="smtp.gmail.com", port="587") as smtp:  # 設定SMTP伺服器
                try:
                    smtp.ehlo()  # 驗證SMTP伺服器
                    smtp.starttls()  # 建立加密傳輸
                    smtp.login("financial@4season.org.tw", "4s@wfinancial")  # 登入寄件者gmail
                    smtp.sendmail(src, [dest], content.as_string())  # 寄送郵件
                    print("Complete!")
                except Exception as e:
                    print("Error message: ", e)

        queryset = Payment.objects.all()
        payment = get_object_or_404(queryset, id=pk)
        email = payment.email
        name = payment.name
        account_number = payment.account_number
        transfer_date = payment.transfer_date
        suffix_obj = {"account_number": account_number, "transfer_date": transfer_date.strftime("%Y-%m-%d"), "id": pk}
        suffix = Fernet(self.key).encrypt(json.dumps(suffix_obj).encode())
        url = f"/invoice/{suffix}"
        print(email, suffix)
        send_mail(
            "financial@4season.org.tw",
            email,
            "四季溯溪營隊收據",
            f"Hi {name},<br><br>"
            f"<div style='padding-left: 30px'>歡迎報名四季溯營隊<br>"
            f"這是您的<a href=http://localhost:3000/Finance/PaymentDetail/{suffix.decode()}> 收據 <a><br>"
            f"請妥善保管此郵件，謝謝。<br><br></div>"
            f"<div style='padding-left: 200px;color:black'>四季財務組</div>",
        )
        return Response(suffix)
