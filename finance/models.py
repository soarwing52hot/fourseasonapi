from django.conf import settings
from django.db import models
from django.utils import timezone

from common.models import GENDER_CHOICES, BaseModel, PaymentBaseModel


class PaymentTypes(models.TextChoices):
    New = "1", "新會員註冊"
    Basic = "2", "會員會費"
    Other = "3", "其他"


class Payment(PaymentBaseModel):
    class Meta:
        db_table = "payment"

    name = models.CharField(max_length=20)
    email = models.EmailField(blank=True)
    phone = models.CharField(max_length=10, blank=True)
    type = models.CharField(max_length=1, choices=PaymentTypes.choices)
    member = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="payment_member", on_delete=models.PROTECT, blank=True, null=True)
    confirm_person = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="payment_confirm", on_delete=models.PROTECT, blank=True, null=True)
