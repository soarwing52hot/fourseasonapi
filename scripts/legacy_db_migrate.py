from user.models import LegacyUser, CustomUser
from django.contrib.auth.hashers import make_password
from django.db.utils import IntegrityError, DataError
from datetime import datetime
from django.core import serializers

User = CustomUser


for legacyUser in LegacyUser.objects.all().order_by('memberNo'):
    print(legacyUser.memberNo, legacyUser.Name)

    try:
        dj = datetime.strptime(f"{str(int(legacyUser.Join_year.replace('.', ''))+1911)}+00:00", '%Y%z')
    except Exception as e:
        dj = datetime.strptime(f"{1991}+00:00", "%Y%z")
        print(e)

    user = User(
            username=legacyUser.memberNo,
            email=legacyUser.Name,
            password=make_password(legacyUser.Password),
            is_active=int(legacyUser.Fee),
            membership_number=legacyUser.memberNo,
            name=legacyUser.Name if len(legacyUser.Name) < 20 else legacyUser.Name[:20],
            gender='M' if legacyUser.Sex == 1 else 'F',
            birth_location='',
            citizen_id=legacyUser.ID_no if len(legacyUser.ID_no) < 20 else legacyUser.ID_no[:20],
            phone=legacyUser.Mobile if len(legacyUser.Mobile) < 10 else "",
            land_line=legacyUser.Phone_home if len(legacyUser.Phone_home) < 10 else "",
            address=legacyUser.Address,
            postal_code=legacyUser.Zipcode if len(legacyUser.Zipcode) < 5 else "",
            emergency_contact_name=legacyUser.E_contact if len(legacyUser.E_contact) < 20 else "",
            emergency_contact_phone=legacyUser.E_phone if len(legacyUser.E_phone) < 30 else "",
            emergency_contact_relation=legacyUser.E_relationship if len(legacyUser.E_relationship) < 10 else "",
            facebook_name=legacyUser.Msn,
            date_joined=dj,
            license=legacyUser.License
        )
    try:
        User.objects.bulk_create([user])
    except IntegrityError as e:
        print(e, 'Duplicate entry, pass...')
        print(serializers.serialize('json', [user]))
    except DataError as e:
        print(e, 'Data too long, pass')
        print(serializers.serialize('json', [user]))
