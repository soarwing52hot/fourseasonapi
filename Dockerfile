FROM python:3.10-alpine
RUN apk update && apk add --virtual build-essential gcc python3-dev musl-dev postgresql-dev
RUN apk update && apk add --virtual build-deps
RUN apk update && apk add --virtual --no-cache mariadb-connector-c-dev

RUN pip install psycopg2-binary
COPY ./requirements.txt .
RUN pip install -r requirements.txt

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 1

WORKDIR /app
COPY . .
RUN python manage.py collectstatic --noinput
RUN adduser -D myuser
RUN chown -R myuser:myuser /app
USER myuser

CMD gunicorn four_season_api.wsgi:application --bind 0.0.0.0:$PORT --timeout 120
