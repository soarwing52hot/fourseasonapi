from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from rest_framework import serializers
from user.models import CustomUser
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class LoginSerializer(TokenObtainPairSerializer):
    membership_number = serializers.IntegerField()
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token["username"] = user.username

        groups = user.groups.all()
        group_info = [str(g) for g in groups]
        group_info = ",".join(group_info)

        token["group"] = group_info
        return token
