from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import GENDER_CHOICES


# Create your views here.
@api_view(["GET"])
def gender_options(request):
    data = {i[0]: i[1] for i in GENDER_CHOICES}
    return Response(data)
