from django.db import models
from django.utils import timezone

GENDER_CHOICES = (("M", "男性"), ("F", "女性"))


class BaseModel(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True, editable=False)
    modify_date = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class PaymentBaseModel(BaseModel):
    class Meta:
        abstract = True

    account_number = models.CharField(max_length=5)
    transfer_date = models.DateField(default=timezone.now)
    memo = models.TextField(blank=True, null=True)
    confirmed = models.BooleanField(default=False)
    amount = models.IntegerField(default=0)
