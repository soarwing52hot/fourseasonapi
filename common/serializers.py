from rest_framework import serializers


class OptionSelectSerialzer(serializers.Serializer):
    value = serializers.SerializerMethodField()
    label = serializers.SerializerMethodField()

    def get_value(self, obj):
        return obj[0]

    def get_label(self, obj):
        return obj[1]


class ResultOutputSerializer(serializers.Serializer):
    message = serializers.CharField()
    success = serializers.BooleanField()
