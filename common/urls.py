from django.urls import path, include
from common import views

urlpatterns = [
    path('gender_options', views.gender_options),
]