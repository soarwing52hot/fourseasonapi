from io import BytesIO

from google.cloud import storage
from PIL import Image


def compress(image_content):
    img = Image.open(BytesIO(image_content))

    compressed_image_content = BytesIO()
    img.save(compressed_image_content, format="JPEG", quality=5)
    return compressed_image_content.getvalue()


def upload_image_to_gcp(blob_name, image):
    image_content = image.read()

    image_content = compress(image_content)
    # 初始化存儲桶客戶端
    client = storage.Client()

    # 獲取指定的存儲桶
    bucket = client.get_bucket("fourseasonweb")

    # 在存儲桶中創建一個新的Blob（對象）
    blob = bucket.blob(f"{blob_name}")

    # 上傳本地檔案到Blob中
    blob.upload_from_string(image_content, content_type=image.content_type)

    return f"https://storage.googleapis.com/{bucket.name}/{blob.name}"


def upload_pdf_to_gcp(blob_name, file):
    file_content = file.read()
    # 初始化存儲桶客戶端
    client = storage.Client()

    # 獲取指定的存儲桶
    bucket = client.get_bucket("fourseasonweb")

    # 在存儲桶中創建一個新的Blob（對象）
    blob = bucket.blob(f"{blob_name}")

    # 上傳本地檔案到Blob中
    blob.upload_from_string(file_content, content_type=file.content_type)
