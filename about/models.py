import datetime

from django.db import models


def current_year():
    return datetime.date.today().year


class Committee(models.Model):
    year = models.IntegerField(default=current_year)
    chairman = models.CharField(max_length=50)
    secratary = models.CharField(max_length=50)
    guide_lead = models.CharField(max_length=50)
    # 名字用逗號分隔
    directors = models.CharField(max_length=300)
    superivsors = models.CharField(max_length=300)


class Video(models.Model):
    title = models.CharField(max_length=50)
    url = models.URLField(max_length=200)
    is_active = models.BooleanField(default=True)
