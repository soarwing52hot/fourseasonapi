from django.contrib import admin

from .models import Committee, Video


# Register your models here.
class CommitteeAdmin(admin.ModelAdmin):
    list_display = ("year",)


admin.site.register(Committee, CommitteeAdmin)


class VideoAdmin(admin.ModelAdmin):
    list_display = ("title",)


admin.site.register(Video, VideoAdmin)
