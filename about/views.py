from django.shortcuts import render
from rest_framework import generics, permissions

from about import serializers
from about.models import Committee, Video

# Create your views here.


class CommitteeView(generics.ListAPIView):
    queryset = Committee.objects.all()
    serializer_class = serializers.CommitteeSerializer
    permission_classes = [permissions.AllowAny]


class VideoView(generics.ListAPIView):
    queryset = Video.objects.filter(is_active=True).all()
    serializer_class = serializers.VideoSerializer
    permission_classes = [permissions.AllowAny]
