from django.urls import include, path

from about import views

urlpatterns = [
    path("committee/", views.CommitteeView.as_view()),
    path("videos/", views.VideoView.as_view()),
]
