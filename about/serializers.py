from rest_framework import serializers

from about.models import Committee, Video


class VideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Video
        fields = "__all__"


class CommitteeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Committee
        fields = "__all__"
