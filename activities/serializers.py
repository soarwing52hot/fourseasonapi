from rest_framework import serializers

from user.serializers import UserDetailDtoSerializer, UserDtoSerializer, UserNameBlockedSerializer

from .models import ActionTypes, Activity, ActivityStamp, IdentityTypes, MemberLog


class ActivitySerializer(serializers.ModelSerializer):
    leaders = UserDtoSerializer(read_only=True, many=True)
    register_available = serializers.SerializerMethodField()

    class Meta:
        model = Activity
        fields = "__all__"

    def create(self, validated_data):
        return Activity.objects.create(**validated_data)

    def get_register_available(self, obj):
        return obj.register_available


class ActivityUserSerializer(serializers.ModelSerializer):
    leaders = UserDtoSerializer(read_only=True, many=True)
    emergency_conductor = UserDtoSerializer(read_only=True, many=True)
    participants = UserDtoSerializer(read_only=True, many=True)
    activity_type_name = serializers.SerializerMethodField()
    activity_requirements_name = serializers.SerializerMethodField()
    register_available = serializers.SerializerMethodField()

    class Meta:
        model = Activity
        fields = "__all__"

    def get_activity_type_name(self, obj):
        return obj.get_activity_type_display()

    def get_activity_requirements_name(self, obj):
        return obj.get_activity_requirements_display()

    def get_register_available(self, obj):
        return obj.register_available


class ActivityUserDetailSerializer(ActivityUserSerializer):
    leaders = UserDetailDtoSerializer(read_only=True, many=True)
    emergency_conductor = UserDetailDtoSerializer(read_only=True, many=True)
    participants = UserDetailDtoSerializer(read_only=True, many=True)
    register_available = serializers.SerializerMethodField()

    def get_register_available(self, obj):
        return obj.register_available


class ActivityPublicSerializer(ActivityUserSerializer):
    leaders = UserNameBlockedSerializer(read_only=True, many=True)
    emergency_conductor = UserNameBlockedSerializer(read_only=True, many=True)
    participants = UserNameBlockedSerializer(read_only=True, many=True)
    register_available = serializers.SerializerMethodField()

    class Meta:
        model = Activity
        exclude = (
            "content",
            "announcement",
        )

    def get_register_available(self, obj):
        return obj.register_available


class ActivityStampSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityStamp
        fields = "__all__"


class ActivityRegisterSerializer(serializers.Serializer):
    membership_numbers = serializers.ListField(child=serializers.IntegerField())
    register = serializers.BooleanField(required=False)
    identity_type = serializers.ChoiceField(choices=IdentityTypes.choices)
    car = serializers.IntegerField(required=False)


class MemberLogSerializer(serializers.ModelSerializer):
    operator = UserDtoSerializer(read_only=True)
    member_number = UserDtoSerializer(read_only=True)
    identity_type_name = serializers.SerializerMethodField()
    action_type_name = serializers.SerializerMethodField()

    class Meta:
        model = MemberLog
        exclude = (
            "identity_type",
            "action_type",
        )

    def get_identity_type_name(self, obj):
        return obj.get_identity_type_display()

    def get_action_type_name(self, obj):
        return obj.get_action_type_display()


class InsuranceSerializer(serializers.Serializer):
    membership_number = serializers.IntegerField()
    register = serializers.BooleanField()


class EmailSerializer(serializers.Serializer):
    activity = serializers.IntegerField()
    subject = serializers.CharField()
    message = serializers.CharField()
