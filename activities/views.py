import calendar
import datetime
import os
from typing import List

import openpyxl
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.db.models import Count
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions, status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from activities import models, serializers
from activities.models import Activity, MemberLog
from activities.permissions import IsLeaderOrReadOnly, LeaderOnly
from common.serializers import OptionSelectSerialzer, ResultOutputSerializer
from user.models import CustomUser
from user.permissions import IsSecretaryOrReadOnly

USER = get_user_model()


class ActivityViewSet(viewsets.ModelViewSet):
    queryset = Activity.objects.all().order_by("trip_date_start")
    serializer_class = serializers.ActivitySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsLeaderOrReadOnly]

    search_fields = ["trip_date_start", "trip_date_end", "title"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filterset_fields = {
        "trip_date_start": ["gte"],
        "trip_date_end": ["lte"],
        "title": ["icontains"],
    }

    def get_serializer_class(self):
        if self.request.method == "GET":
            if not self.request.user.is_authenticated:
                return serializers.ActivityPublicSerializer
            if not "pk" in self.kwargs:
                return serializers.ActivityUserSerializer

            if LeaderOnly().has_object_permission(self.request, self, self.get_object()):
                return serializers.ActivityUserDetailSerializer
            return serializers.ActivityUserSerializer
        else:
            return serializers.ActivitySerializer

    def perform_create(self, serializer):
        created_activity = serializer.save()
        created_activity.leaders.add(self.request.user)


class ActivityRecordSearchView(generics.ListAPIView):
    queryset = Activity.objects.all().order_by("-trip_date_start")
    serializer_class = serializers.ActivitySerializer
    permission_classes = [permissions.IsAuthenticated]
    search_fields = ["trip_date_start", "trip_date_end", "leaders__membership_number", "title", "participants__membership_number"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filterset_fields = {
        "trip_date_start": ["gte"],
        "trip_date_end": ["lte"],
        "leaders__membership_number": ["exact"],
        "title": ["icontains"],
        "participants__membership_number": ["exact"],
    }


class MemberLogListView(generics.ListAPIView):
    queryset = MemberLog.objects.all().order_by("-modify_date")
    serializer_class = serializers.MemberLogSerializer
    permission_classes = [permissions.AllowAny]
    search_fields = ["activity__id"]
    filterset_fields = {"activity__id": ["exact"]}


class ActivityStampView(generics.ListAPIView):
    queryset = models.ActivityStamp.objects.all()
    serializer_class = serializers.ActivityStampSerializer


class ActivityRegisterView(generics.GenericAPIView):
    queryset = Activity.objects.all()
    serializer_class = serializers.ActivityRegisterSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def put(self, request, pk):
        identity_type = request.data.get("identity_type", "P")
        join_activity = request.data.get("register")
        car = request.data.get("car", 0)
        action_type = models.ActionTypes.Add if join_activity else models.ActionTypes.Remove

        membership_numbers = request.data.get("membership_numbers")
        members: List[CustomUser] = USER.objects.filter(membership_number__in=membership_numbers)

        activity: Activity = self.get_object()
        logs = models.MemberLog.objects.filter(activity=activity)
        leaders_id = activity.leaders.values_list("membership_number", flat=True)
        if activity.register_available is False and request.user.membership_number not in leaders_id:
            return Response({"message": "報名失敗，此活動已無法報名"}, status=500)

        if request.user.membership_number != membership_numbers[0] and request.user.membership_number not in leaders_id:
            return Response({"message": "非領隊不能改他人狀態"}, status=500)
        logMap = {}
        for log in logs:
            logMap[log.member_number.membership_number] = log

        recipient_list = []
        for member in members:
            member_log = logMap.get(member.membership_number)
            if member_log is None:
                member_log = models.MemberLog(activity=activity, member_number=member, operator=request.user)

            if join_activity is True:
                member_log.join_date = datetime.datetime.now()
                if identity_type == models.IdentityTypes.Participant:
                    leader_ids = activity.leaders.values_list("membership_number", flat=True)
                    if member.membership_number in leader_ids:
                        return Response({"message": "報名失敗，已經在領隊名單中!"}, status=500)

                    if not activity.is_emergency and not member.paid_this_year:
                        return Response({"message": "報名失敗，有會員尚未繳會費喔! 只有緊急召集可以加入非當年會員"}, status=500)

                    activity.participants.add(member)
                elif identity_type == models.IdentityTypes.Leader:
                    activity.leaders.add(member)
                elif identity_type == models.IdentityTypes.EmergencyConductor:
                    activity.emergency_conductor.add(member)
            elif join_activity is False:
                if identity_type == models.IdentityTypes.Participant:
                    activity.participants.remove(member)
                elif identity_type == models.IdentityTypes.Leader:
                    activity.leaders.remove(member)
                elif identity_type == models.IdentityTypes.EmergencyConductor:
                    activity.emergency_conductor.remove(member)

            member_log.operator = request.user
            member_log.action_type = action_type
            member_log.identity_type = identity_type

            if car > 0:
                member_log.car = car

            member_log.save()

            recipient_list.append(member.email)

        if activity.notice_leader:
            leaders_email = activity.leaders.values_list("email", flat=True)
            leaders_email = list(leaders_email)
            recipient_list += leaders_email
        try:
            from_email = settings.EMAIL_HOST_USER
            subject = "成員異動"
            subject = f"四季行程領隊公用信箱通知信(請勿回信) - {activity.title} - {subject}"
            action_string = "加入" if join_activity is True else "退出"
            message = f"""會員{member.membership_number}{action_string}隊伍{activity.title}"""
            # 如果是領隊自己送 不應該整個列表都寄信
            send_mail(subject, message, from_email, recipient_list)
        except Exception as ex:
            print(ex)

        return Response(serializers.ActivitySerializer(activity).data)


class InsuranceView(generics.RetrieveAPIView):
    queryset = Activity.objects.all().prefetch_related("participants")
    serializer_class = serializers.ActivitySerializer
    permission_classes = [LeaderOnly]

    def retrieve(self, request, pk):
        wb_obj = openpyxl.Workbook()
        sheet = wb_obj.active
        activity: models.Activity = self.get_object()
        all_list = list(activity.leaders.all()) + list(activity.participants.all())
        for index, user in enumerate(all_list, start=1):
            sheet[f"A{index}"] = user.username
            sheet[f"B{index}"] = user.citizen_id
            sheet[f"C{index}"] = user.birth_date
            sheet[f"D{index}"] = 300

        temp_file_name = f"insurance_{pk}.xlsx"
        temp_file_full_path = f"activities/{temp_file_name}"
        wb_obj.save(temp_file_full_path)
        document = open(temp_file_full_path, "rb")
        response = HttpResponse(document, content_type="application/msexcel")
        response["Content-Disposition"] = f"attachment; filename={temp_file_name}"
        os.remove(temp_file_full_path)
        return response


class NameListView(generics.RetrieveAPIView):
    queryset = Activity.objects.all().prefetch_related("participants")
    serializer_class = serializers.ActivitySerializer
    permission_classes = [LeaderOnly]

    def retrieve(self, request, pk):
        wb_obj = openpyxl.Workbook()
        sheet = wb_obj.active
        activity: models.Activity = self.get_object()
        all_list: List[CustomUser] = list(activity.leaders.all()) + list(activity.participants.all())
        headers = (
            ("A1", "會員編號"),
            ("B1", "姓名"),
            ("C1", "性別"),
            ("D1", "出生日期"),
            ("E1", "身分證字號"),
            ("F1", "電話"),
            ("G1", "電子郵件"),
            ("H1", "地址"),
            ("I1", "緊急聯絡人姓名"),
            ("J1", "緊急聯絡人電話"),
            ("K1", "緊急聯絡人關係"),
            ("L1", "幹部身分"),
        )
        for col, val in headers:
            sheet[col] = val

        for index, user in enumerate(all_list, start=2):
            sheet[f"A{index}"] = user.membership_number
            sheet[f"B{index}"] = user.username
            sheet[f"C{index}"] = user.get_gender_display()
            sheet[f"D{index}"] = user.birth_date
            sheet[f"E{index}"] = user.citizen_id
            sheet[f"F{index}"] = user.phone
            sheet[f"G{index}"] = user.email
            sheet[f"H{index}"] = user.address
            sheet[f"I{index}"] = user.emergency_contact_name
            sheet[f"J{index}"] = user.emergency_contact_phone
            sheet[f"K{index}"] = user.emergency_contact_relation
            sheet[f"L{index}"] = "O" if user.is_associate() else "X"

        temp_file_name = f"namelist_{pk}.xlsx"
        temp_file_full_path = f"activities/{temp_file_name}"
        wb_obj.save(temp_file_full_path)
        document = open(temp_file_full_path, "rb")
        response = HttpResponse(document, content_type="application/msexcel")
        response["Content-Disposition"] = f"attachment; filename={temp_file_name}"
        os.remove(temp_file_full_path)
        return response


class ActivityCountView(generics.RetrieveAPIView):
    permission_classes = [IsSecretaryOrReadOnly]

    def retrieve(self, request, year):
        result = {}
        for i in range(1, 13):
            activities_count = Activity.objects.filter(active=True, trip_date_start__year=year, trip_date_start__month=i).count()
            result[calendar.month_name[i]] = activities_count

        return Response(result)


class ActivityParticipantCountView(generics.RetrieveAPIView):
    permission_classes = [IsSecretaryOrReadOnly]

    def retrieve(self, request, year):
        result = {}
        for i in range(1, 13):
            participant_count = 0
            activities = Activity.objects.filter(active=True, trip_date_start__year=year, trip_date_start__month=i).annotate(participant_count=Count("participants"))
            for activity in activities:
                participant_count += activity.participant_count
            result[calendar.month_name[i]] = participant_count

        return Response(result)


class ParticipantChartView(generics.RetrieveAPIView):
    permission_classes = [IsSecretaryOrReadOnly]

    def retrieve(self, request, year):
        result = {}
        for i in range(1, 13):
            activities = Activity.objects.filter(active=True, trip_date_start__year=year, trip_date_start__month=i).prefetch_related("participants")
            for activity in activities:
                for p in activity.participants.all():
                    value = result.get(p.username, 0)
                    value += 1
                    result[p.username] = value
        # TODO:扣掉幹部身分 要怎麼檢查幹部年度?
        result = sorted(result.items(), key=lambda x: x[1], reverse=True)
        return Response(result)


class LeaderChartView(generics.RetrieveAPIView):
    permission_classes = [IsSecretaryOrReadOnly]

    def retrieve(self, request, year):
        result = {}
        for i in range(1, 13):
            activities = Activity.objects.filter(active=True, trip_date_start__year=year, trip_date_start__month=i).prefetch_related("leaders")
            for activity in activities:
                for p in activity.leaders.all():
                    value = result.get(p.username, 0)
                    value += 1
                    result[p.username] = value
        # TODO:扣掉幹部身分 要怎麼檢查幹部年度?
        response: List = sorted(result.items(), key=lambda x: x[1], reverse=True)
        return Response(response)


class EmailView(APIView):
    permission_classes = [LeaderOnly]

    def post(self, request, format=None):
        serializer = serializers.EmailSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if serializer.validated_data is None and not serializer.validated_data:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        activity = get_object_or_404(Activity, id=serializer.validated_data["activity"])

        recipient_list = activity.participants.values_list("email", flat=True)
        leaders_email = activity.leaders.values_list("email", flat=True)
        recipient_list = list(leaders_email) + list(recipient_list)

        subject = serializer.validated_data["subject"]
        message = serializer.validated_data["message"]
        from_email = settings.EMAIL_HOST_USER

        subject = f"四季行程領隊公用信箱通知信(請勿回信) - {subject}"

        send_mail(subject, message, from_email, recipient_list)
        output_serializer = ResultOutputSerializer({"message": "Email sent", "success": True})
        return Response(output_serializer.data, status=status.HTTP_200_OK)


class ActivityTypeOptionsAPIView(APIView):
    def get(self, request):
        camp_types = models.ActivityTypes.choices
        serializer = OptionSelectSerialzer(camp_types, many=True)
        return Response(serializer.data)


class ActivityRequirementOptionsAPIView(APIView):
    def get(self, request):
        camp_types = models.ActivityRequirements.choices
        serializer = OptionSelectSerialzer(camp_types, many=True)
        return Response(serializer.data)
