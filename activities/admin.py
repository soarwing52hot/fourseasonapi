from django.contrib import admin
from .models import Activity, ActivityStamp

# Register your models here.
class ActivityAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "trip_date_start",
        "trip_date_end",
    )


admin.site.register(Activity, ActivityAdmin)


class ActivityStampAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


admin.site.register(ActivityStamp, ActivityStampAdmin)
