from rest_framework import permissions


class IsLeaderOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        leader_ids = [leader.membership_number for leader in obj.leaders.all()]

        if request.user.membership_number in leader_ids:
            return True


class LeaderOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.filter(name="Leader").exists()

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        leader_ids = [leader.membership_number for leader in obj.leaders.all()]
        if request.user.membership_number in leader_ids:
            return True

        emergency_conductors = [eme.membership_number for eme in obj.emergency_conductor.all()]
        if request.user.membership_number in emergency_conductors:
            return True
