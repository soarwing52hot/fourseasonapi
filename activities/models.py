from datetime import date

from django.conf import settings
from django.db import models
from django.utils import timezone

from common.models import BaseModel
from user.models import CustomUser


class ActivityTypes(models.TextChoices):
    Meetup = "MU", "聚會"
    Train = "T", "訓練"
    Suburb = "S", "近郊溪行程(Suburb)"
    Middle = "M", "次高中游溪行程(Middle)"
    Alpine = "A", "高山溪行程(Alpine)"


class ActivityRequirements(models.TextChoices):
    NoRequirements = "N", "不限基本資格"
    Member = "Mem", "會員(巳繳納本年度會費者)"
    Experience = "E", "溯溪體驗營結訓者"
    Basic = "B", "會員且初級溯溪營結訓"
    Advanced = "Adv", "會員且進階營結訓"
    Suburb = "S", "近郊溪行程(Suburb)"
    Middle = "M", "次高中游溪行程(Middle)"
    Alpine = "A", "高山溪行程(Alpine)"
    Source = "So", "溯源(Source)"
    Top = "T", "溯源且登頂(Top)"


class ActivityStamp(models.Model):
    class Meta:
        ordering = ["id"]

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Activity(models.Model):
    class Meta:
        db_table = "activity"
        ordering = ["id"]

    title = models.CharField(max_length=50)
    content = models.CharField(max_length=100, blank=True)
    active = models.BooleanField(default=False)

    is_test = models.BooleanField(default=False)
    is_emergency = models.BooleanField(default=False)
    notice_leader = models.BooleanField(default=False)
    leaders = models.ManyToManyField(CustomUser, related_name="leaders", blank=True)
    emergency_conductor = models.ManyToManyField(CustomUser, related_name="emergency_conductor", blank=True)
    participants = models.ManyToManyField(CustomUser, related_name="participants", blank=True)
    announcement = models.IntegerField(blank=True)

    stamp = models.ManyToManyField(ActivityStamp, blank=True)

    created_on = models.DateField(default=date.today)

    can_register = models.BooleanField(default=False)
    register_end = models.DateTimeField(default=timezone.now)

    trip_date_start = models.DateField(default=timezone.now, blank=True)
    trip_date_end = models.DateField(default=timezone.now, blank=True)

    activity_type = models.CharField(max_length=5, choices=ActivityTypes.choices, default=ActivityTypes.Suburb, blank=True, null=True)

    activity_requirements = models.CharField(max_length=5, choices=ActivityRequirements.choices, default=ActivityRequirements.Member)
    activity_requirements_count = models.IntegerField(default=0)

    participant_limit = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    @property
    def register_available(self):
        # 領隊打勾開放報名權限最大
        if self.can_register is False:
            return False

        # 如果活動已取消也不能更動
        if self.active is False:
            return False
        # 看報名日期
        if timezone.now() > self.register_end:
            return False

        # 活動開始後不得報名
        if timezone.now().date() > self.trip_date_start:
            return False

        # 接下來看人數有沒有超過
        if self.participant_limit == 0:
            return True

        if self.participants.count() >= self.participant_limit:
            return False

        return True


class IdentityTypes(models.TextChoices):
    Leader = "L", "領隊"
    Participant = "P", "隊員"
    EmergencyConductor = "E", "留守人"


class ActionTypes(models.TextChoices):
    Add = "A", "加入"
    Remove = "R", "退出"


class MemberLog(BaseModel):
    activity = models.ForeignKey(Activity, on_delete=models.PROTECT)
    operator = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="activity_member_operator", on_delete=models.PROTECT)
    member_number = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="activity_log_member", on_delete=models.PROTECT)
    action_type = models.CharField(max_length=5, choices=ActionTypes.choices)
    identity_type = models.CharField(max_length=5, choices=IdentityTypes.choices)
    join_date = models.DateTimeField(blank=True, null=True)
    car = models.IntegerField(blank=True, default=0)
