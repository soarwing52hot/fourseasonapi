# Generated by Django 3.2.9 on 2024-02-23 05:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('activities', '0018_alter_activity_activity_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='MemberLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('modify_date', models.DateTimeField(auto_now=True)),
                ('action_type', models.CharField(choices=[('A', '加入'), ('R', '移除')], max_length=5)),
                ('identity_type', models.CharField(choices=[('L', '領隊'), ('P', '隊員')], max_length=5)),
                ('activity', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='activities.activity')),
                ('member_number', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='activity_log_member', to=settings.AUTH_USER_MODEL)),
                ('operator', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='activity_member_operator', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
