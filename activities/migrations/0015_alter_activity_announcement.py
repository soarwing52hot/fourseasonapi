# Generated by Django 3.2.9 on 2023-09-01 07:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0014_auto_20230804_1618'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='announcement',
            field=models.IntegerField(blank=True),
        ),
    ]
