# Generated by Django 3.2.9 on 2023-12-08 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0017_activity_notice_leader'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='activity_type',
            field=models.CharField(blank=True, choices=[('MU', '聚會'), ('T', '訓練'), ('S', '近郊溪行程(Suburb)'), ('M', '次高中游溪行程(Middle)'), ('A', '高山溪行程(Alpine)')], default='S', max_length=5, null=True),
        ),
    ]
