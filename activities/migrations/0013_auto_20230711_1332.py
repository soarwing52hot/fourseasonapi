# Generated by Django 3.2.9 on 2023-07-11 05:32

import datetime
from django.db import migrations, models
from django.utils.timezone import utc
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0012_activity_announcement'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='activity_requirements',
            field=models.CharField(blank=True, choices=[('N', '不限基本資格'), ('Mem', '會員(巳繳納本年度會費者)'), ('E', '溯溪體驗營結訓者'), ('B', '會員且初級溯溪營結訓'), ('Adv', '會員且進階營結訓'), ('S', '近郊溪行程(Suburb)'), ('M', '次高中游溪行程(Middle)'), ('A', '高山溪行程(Alpine)'), ('So', '溯源(Source)'), ('T', '溯源且登頂(Top)')], default='Mem', max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='activity_requirements_count',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='activity_type',
            field=models.CharField(blank=True, choices=[('S', '近郊溪行程(Suburb)'), ('M', '次高中游溪行程(Middle)'), ('A', '高山溪行程(Alpine)')], default='S', max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='announcement',
            field=models.CharField(blank=True, default=datetime.datetime(2023, 7, 11, 5, 32, 27, 25385, tzinfo=utc), max_length=250),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='activity',
            name='content',
            field=models.CharField(blank=True, default='content', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='activity',
            name='participant_limit',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='trip_date_end',
            field=models.DateField(blank=True, default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='activity',
            name='trip_date_start',
            field=models.DateField(blank=True, default=django.utils.timezone.now),
        ),
    ]
