from django.urls import include, path
from rest_framework.routers import DefaultRouter

from activities import views

router = DefaultRouter()
router.register(r"info", views.ActivityViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("register/<int:pk>/", views.ActivityRegisterView.as_view()),
    path("insurance/<int:pk>", views.InsuranceView.as_view()),
    path("nameList/<int:pk>", views.NameListView.as_view()),
    path("record/", views.ActivityRecordSearchView.as_view()),
    path("memberLog/", views.MemberLogListView.as_view()),
    path("activityCount/<int:year>", views.ActivityCountView.as_view()),
    path("activityParticipantCount/<int:year>", views.ActivityParticipantCountView.as_view()),
    path("participantChart/<int:year>", views.ParticipantChartView.as_view()),
    path("leaderChart/<int:year>", views.LeaderChartView.as_view()),
    path("sendEmail/", views.EmailView.as_view(), name="send-email"),
    # 固定選項
    path("options/stamps/", views.ActivityStampView.as_view()),
    path("options/types/", views.ActivityTypeOptionsAPIView.as_view(), name="camp_types"),
    path("options/requirements/", views.ActivityRequirementOptionsAPIView.as_view(), name="camp_types"),
]
