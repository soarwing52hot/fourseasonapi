from django.urls import path
from legacy import views

urlpatterns = [
    path("schedule/<int:schedule_id>", views.legacy_schedule_view_by_id, name="schedule_by_id"),
    path("schedule",
         views.legacy_schedule_paging_view,
         name="schedule_paging"
         ),
]