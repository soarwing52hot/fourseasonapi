import json
from typing import List

from legacy.models import Schedule, Member


class LegacyScheduleDto:
    def __init__(self, schedule: Schedule,
                 leader_list: List[Member],
                 member_list: List[Member]):
        self.schedule: Schedule = schedule
        self.leader_list: List[Member] = leader_list
        self.member_list: List[Member] = member_list

    def __str__(self) -> str:
        dto_dict: dict = json.loads(str(self.schedule))
        dto_dict['leader_list'] = [json.loads(str(leader)) for leader in self.leader_list]
        dto_dict['member_list'] = [json.loads(str(member)) for member in self.member_list]
        return json.dumps(dto_dict, ensure_ascii=False)


class LegacyScheduleListDto:
    def __init__(
            self,
            schedule_dto_list: List[LegacyScheduleDto],
            size: int = None,
            page: int = None,
            total_pages: int = None,
            total_count: int = None
                 ):
        self.size = size
        self.page = page
        self.total_pages = total_pages
        self.total_count = total_count
        self.schedule_dto_list = schedule_dto_list
        # page_info

    def __str__(self) -> str:
        schedule_data: List[str] = []
        for schedule in self.schedule_dto_list:
            schedule_data.append(json.loads(str(schedule)))

        return json.dumps(
            {
                'size': self.size,
                'page': self.page,
                'total_pages': self.total_pages,
                'total_count': self.total_count,
                'schedule_list': schedule_data
            }, ensure_ascii=False)
