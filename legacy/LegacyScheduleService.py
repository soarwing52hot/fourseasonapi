from typing import List, Type

from django.core.paginator import Paginator

from legacy.dto import LegacyScheduleDto, LegacyScheduleListDto
from legacy.models import Schedule, Member, ScheduleLeader, Join


def build_legacy_schedule_dto(schedule: Schedule) -> LegacyScheduleDto:
    schedule_leader_memberno_list: List[str] = ScheduleLeader.objects.filter(fid=schedule.id).values('memberno')
    member_memberno_list: List[str] = Join.objects.filter(fid=schedule.id).values('memberno')

    leader_list: List[Member] = Member.objects.filter(memberno__in=schedule_leader_memberno_list)
    member_list: List[Member] = Member.objects.filter(memberno__in=member_memberno_list)

    schedule_dto: LegacyScheduleDto = LegacyScheduleDto(schedule,
                                                        leader_list,
                                                        member_list)
    return schedule_dto


def paging_legacy_schedule_list_dto(schedule_list: List[Schedule], size: int, page: int) -> LegacyScheduleListDto:
    paginator: Paginator = Paginator(schedule_list, size)
    schedule_list: List[Schedule] = paginator.get_page(page)
    schedule_dto_list: List[LegacyScheduleDto] = []
    for schedule in schedule_list:
        schedule_dto: LegacyScheduleDto = build_legacy_schedule_dto(schedule)
        schedule_dto_list.append(schedule_dto)
    total_pages: int = paginator.num_pages
    total_count: int = paginator.count
    legacy_schedule_list_dto: LegacyScheduleListDto = LegacyScheduleListDto(schedule_dto_list,
                                                                            size,
                                                                            page,
                                                                            total_pages,
                                                                            total_count)
    return legacy_schedule_list_dto


def get_all_schedules(size: int, page: int) -> LegacyScheduleListDto:
    schedule_list: List[Schedule] = Schedule.objects.all().order_by('-date_start')
    return paging_legacy_schedule_list_dto(schedule_list, size, page)


def get_all_schedules_by_leader_id(size: int, page: int, leader_id: str) -> LegacyScheduleListDto:
    model = ScheduleLeader
    return get_all_schedules_by_model_id(size, page, model, leader_id)


def get_all_schedules_by_member_id(size: int, page: int, member_id: str) -> LegacyScheduleListDto:
    model = Join
    return get_all_schedules_by_model_id(size, page, model, member_id)


def get_all_schedules_by_model_id(size: int, page: int, model: Type[Join|ScheduleLeader], member_id: str) -> LegacyScheduleListDto:
    joins: List[model] = model.objects.filter(memberno=member_id)
    member_fid_list: List[int] = [join.fid for join in joins if join.fid != '']
    schedule_list: List[Schedule] = Schedule.objects.filter(id__in=member_fid_list).order_by('-date_start')
    return paging_legacy_schedule_list_dto(schedule_list, size, page)


def get_schedule_dto_by_id(schedule_id: int) -> LegacyScheduleDto:
    schedule: Schedule = Schedule.objects.get(id=schedule_id)

    schedule_dto: LegacyScheduleDto = build_legacy_schedule_dto(schedule)

    return schedule_dto
