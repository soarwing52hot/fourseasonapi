# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
import json
from typing import List

from django.db import models


class Award(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.
    number_5lake_sonlo = models.CharField(db_column='5lake_sonlo', max_length=4)  # Field renamed because it wasn't a valid Python identifier.
    number_5lake_gialo = models.CharField(db_column='5lake_gialo', max_length=4)  # Field renamed because it wasn't a valid Python identifier.
    number_5lake_yuanyan = models.CharField(db_column='5lake_yuanyan', max_length=4)  # Field renamed because it wasn't a valid Python identifier.
    number_5lake_sanmi = models.CharField(db_column='5lake_sanmi', max_length=4)  # Field renamed because it wasn't a valid Python identifier.
    number_5lake_tseifeng = models.CharField(db_column='5lake_tseifeng', max_length=4)  # Field renamed because it wasn't a valid Python identifier.
    group_hotspring = models.CharField(db_column='Group_hotspring', max_length=4)  # Field name made lowercase.
    hotspring_malin = models.CharField(db_column='Hotspring_malin', max_length=4)  # Field name made lowercase.
    schedule_open = models.CharField(db_column='Schedule_open', max_length=4)  # Field name made lowercase.
    schedule_day = models.CharField(db_column='Schedule_day', max_length=4)  # Field name made lowercase.
    schedule_join = models.CharField(db_column='Schedule_join', max_length=4)  # Field name made lowercase.
    schedule_joinday = models.CharField(db_column='Schedule_joinday', max_length=4)  # Field name made lowercase.
    howhen_kubayan = models.CharField(db_column='Howhen_kubayan', max_length=1)  # Field name made lowercase.
    howhen_leiboke = models.CharField(db_column='Howhen_leiboke', max_length=1)  # Field name made lowercase.
    howhen_gianchin = models.CharField(db_column='Howhen_gianchin', max_length=1)  # Field name made lowercase.
    howhen_kalabo = models.CharField(db_column='Howhen_kalabo', max_length=1)  # Field name made lowercase.
    howhen_dibanyinbridge = models.CharField(db_column='Howhen_dibanyinbridge', max_length=1)  # Field name made lowercase.
    howhen_zweilu = models.CharField(db_column='Howhen_zweilu', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'award'
        unique_together = (('id', 'memberno'),)


class Bbs1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    collect = models.CharField(db_column='Collect', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    block = models.CharField(db_column='Block', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    remove = models.CharField(max_length=4)
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bbs1'


class Bbs2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    collect = models.CharField(db_column='Collect', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    block = models.CharField(db_column='Block', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    remove = models.CharField(max_length=4)
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bbs2'


class Bbs3(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    collect = models.CharField(db_column='Collect', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    block = models.CharField(db_column='Block', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    remove = models.CharField(max_length=4)
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bbs3'


class Bbs4(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    collect = models.CharField(db_column='Collect', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    block = models.CharField(db_column='Block', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    remove = models.CharField(max_length=4)
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bbs4'


class Debug(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    ip = models.CharField(db_column='Ip', max_length=16)  # Field name made lowercase.
    log_date = models.CharField(db_column='Log_date', max_length=32)  # Field name made lowercase.
    ql = models.CharField(db_column='Ql', max_length=256)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'debug'


class Fee(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.
    date_year = models.CharField(db_column='Date_year', max_length=3)  # Field name made lowercase.
    fee = models.CharField(db_column='Fee', max_length=4)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'fee'
        unique_together = (('id', 'memberno'),)


class Join(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fid = models.CharField(db_column='FID', max_length=8)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=16)  # Field name made lowercase.
    date_join = models.CharField(db_column='Date_join', max_length=20)  # Field name made lowercase.
    team_join = models.CharField(db_column='Team_join', max_length=4)  # Field name made lowercase.
    role_join = models.CharField(db_column='Role_join', max_length=4)  # Field name made lowercase.
    car_join = models.CharField(db_column='Car_join', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'join'
        unique_together = (('id', 'memberno'),)


class JoinSch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fid = models.CharField(db_column='FID', max_length=8, blank=True, null=True)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=16, blank=True, null=True)  # Field name made lowercase.
    date_join = models.CharField(db_column='Date_join', max_length=20, blank=True, null=True)  # Field name made lowercase.
    team_join = models.CharField(db_column='Team_join', max_length=4, blank=True, null=True)  # Field name made lowercase.
    role_join = models.CharField(db_column='Role_join', max_length=4, blank=True, null=True)  # Field name made lowercase.
    car_join = models.CharField(db_column='Car_join', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'join_sch'


class JoinTeam(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fid = models.CharField(db_column='FID', max_length=8)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=16)  # Field name made lowercase.
    date_join = models.CharField(db_column='Date_join', max_length=20)  # Field name made lowercase.
    team_join = models.CharField(db_column='Team_join', max_length=4)  # Field name made lowercase.
    role_join = models.CharField(db_column='Role_join', max_length=4)  # Field name made lowercase.
    car_join = models.CharField(db_column='Car_join', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'join_team'
        unique_together = (('id', 'memberno'),)


class Login(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=24)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=8)  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date')  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'login'


class Member(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
    license = models.CharField(db_column='License', max_length=8)  # Field name made lowercase.
    epaper = models.PositiveIntegerField(db_column='Epaper')  # Field name made lowercase.
    sex = models.PositiveIntegerField(db_column='Sex')  # Field name made lowercase.
    bd = models.CharField(db_column='Bd', max_length=12)  # Field name made lowercase.
    id_no = models.CharField(db_column='ID_no', max_length=16)  # Field name made lowercase.
    e_contact = models.CharField(db_column='E_contact', max_length=8)  # Field name made lowercase.
    e_relationship = models.CharField(db_column='E_relationship', max_length=16)  # Field name made lowercase.
    e_phone = models.CharField(db_column='E_phone', max_length=32)  # Field name made lowercase.
    fax = models.CharField(db_column='Fax', max_length=16)  # Field name made lowercase.
    phone_home = models.CharField(db_column='Phone_home', max_length=32)  # Field name made lowercase.
    phone_office = models.CharField(db_column='Phone_office', max_length=32)  # Field name made lowercase.
    mobile = models.CharField(db_column='Mobile', max_length=12)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=40)  # Field name made lowercase.
    skype = models.CharField(db_column='Skype', max_length=32)  # Field name made lowercase.
    msn = models.CharField(db_column='Msn', max_length=40)  # Field name made lowercase.
    b_place = models.CharField(db_column='B_place', max_length=16)  # Field name made lowercase.
    b_zipcode = models.CharField(db_column='B_zipcode', max_length=8)  # Field name made lowercase.
    b_address = models.CharField(db_column='B_address', max_length=128)  # Field name made lowercase.
    address = models.TextField(db_column='Address')  # Field name made lowercase.
    zipcode = models.CharField(db_column='Zipcode', max_length=8)  # Field name made lowercase.
    join_year = models.CharField(db_column='Join_year', max_length=4)  # Field name made lowercase.
    study = models.CharField(db_column='Study', max_length=16)  # Field name made lowercase.
    work_past = models.CharField(db_column='Work_past', max_length=64)  # Field name made lowercase.
    work_now = models.CharField(db_column='Work_now', max_length=64)  # Field name made lowercase.
    nickname = models.CharField(db_column='Nickname', max_length=24)  # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=16, db_collation='utf8_bin')  # Field name made lowercase.
    id_webmaster = models.PositiveIntegerField(db_column='ID_webmaster')  # Field name made lowercase.
    id_director = models.PositiveIntegerField(db_column='ID_director')  # Field name made lowercase.
    id_secretary = models.PositiveIntegerField(db_column='ID_secretary')  # Field name made lowercase.
    id_vicesecretary = models.IntegerField(db_column='ID_vicesecretary', blank=True, null=True)  # Field name made lowercase.
    id_admin_director = models.IntegerField(db_column='ID_admin_director', blank=True, null=True)  # Field name made lowercase.
    id_financial = models.IntegerField(db_column='ID_financial', blank=True, null=True)  # Field name made lowercase.
    id_supervisor = models.IntegerField(db_column='ID_supervisor', blank=True, null=True)  # Field name made lowercase.
    id_admin = models.IntegerField(db_column='ID_admin', blank=True, null=True)  # Field name made lowercase.
    id_leader = models.IntegerField(db_column='ID_leader', blank=True, null=True)  # Field name made lowercase.
    id_data = models.IntegerField(db_column='ID_data', blank=True, null=True)  # Field name made lowercase.
    mail_yesno = models.IntegerField(db_column='Mail_yesno', blank=True, null=True)  # Field name made lowercase.
    id_agent_schedule = models.IntegerField(db_column='ID_agent_schedule', blank=True, null=True)  # Field name made lowercase.
    id_agent_money10 = models.IntegerField(db_column='ID_agent_money10', blank=True, null=True)  # Field name made lowercase.
    id_agent_keymember = models.IntegerField(db_column='ID_agent_keymember', blank=True, null=True)  # Field name made lowercase.
    g_account = models.CharField(db_column='G_account', max_length=32)  # Field name made lowercase.
    g_password = models.CharField(db_column='G_password', max_length=32)  # Field name made lowercase.
    fee = models.PositiveIntegerField(db_column='Fee', blank=True, null=True)  # Field name made lowercase.
    email_schedule = models.CharField(db_column='Email_schedule', max_length=32)  # Field name made lowercase.
    id_equipment = models.IntegerField(db_column='ID_equipment')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'member'
        unique_together = (('id', 'memberno'),)

    def masking(self, columns_to_mask: List[str]):
        for column in columns_to_mask:
            if column == 'id' or  column == 'memberno':
                setattr(self, column, '0000')
            else:
                setattr(self, column, '***')


    def __str__(self) -> str:
        ret_dict: dict = {
            'id': self.id,
            'memberno': self.memberno,
            'name': self.name,
            'license': self.license,
            'email': self.email,
            'mobile': self.mobile,
            'sex': self.sex
        }
        return json.dumps(ret_dict, ensure_ascii=False)


class MemberTeam(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=8)  # Field name made lowercase.
    license = models.CharField(db_column='License', max_length=8)  # Field name made lowercase.
    epaper = models.PositiveIntegerField(db_column='Epaper')  # Field name made lowercase.
    sex = models.PositiveIntegerField(db_column='Sex')  # Field name made lowercase.
    bd = models.CharField(db_column='Bd', max_length=12)  # Field name made lowercase.
    id_no = models.CharField(db_column='ID_no', max_length=16)  # Field name made lowercase.
    e_contact = models.CharField(db_column='E_contact', max_length=8)  # Field name made lowercase.
    e_relationship = models.CharField(db_column='E_relationship', max_length=16)  # Field name made lowercase.
    e_phone = models.CharField(db_column='E_phone', max_length=32)  # Field name made lowercase.
    fax = models.CharField(db_column='Fax', max_length=16)  # Field name made lowercase.
    phone_home = models.CharField(db_column='Phone_home', max_length=32)  # Field name made lowercase.
    phone_office = models.CharField(db_column='Phone_office', max_length=32)  # Field name made lowercase.
    mobile = models.CharField(db_column='Mobile', max_length=12)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=40)  # Field name made lowercase.
    skype = models.CharField(db_column='Skype', max_length=32)  # Field name made lowercase.
    msn = models.CharField(db_column='Msn', max_length=40)  # Field name made lowercase.
    b_zipcode = models.CharField(db_column='B_zipcode', max_length=8)  # Field name made lowercase.
    b_address = models.CharField(db_column='B_address', max_length=128)  # Field name made lowercase.
    address = models.TextField(db_column='Address')  # Field name made lowercase.
    zipcode = models.CharField(db_column='Zipcode', max_length=8)  # Field name made lowercase.
    join_year = models.CharField(db_column='Join_year', max_length=4)  # Field name made lowercase.
    study = models.CharField(db_column='Study', max_length=16)  # Field name made lowercase.
    work_past = models.CharField(db_column='Work_past', max_length=64)  # Field name made lowercase.
    work_now = models.CharField(db_column='Work_now', max_length=64)  # Field name made lowercase.
    nickname = models.CharField(db_column='Nickname', max_length=24)  # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=16, db_collation='utf8_bin')  # Field name made lowercase.
    id_webmaster = models.PositiveIntegerField(db_column='ID_webmaster')  # Field name made lowercase.
    id_director = models.PositiveIntegerField(db_column='ID_director')  # Field name made lowercase.
    id_secretary = models.PositiveIntegerField(db_column='ID_secretary')  # Field name made lowercase.
    id_vicesecretary = models.IntegerField(db_column='ID_vicesecretary', blank=True, null=True)  # Field name made lowercase.
    id_admin_director = models.IntegerField(db_column='ID_admin_director', blank=True, null=True)  # Field name made lowercase.
    id_financial = models.IntegerField(db_column='ID_financial', blank=True, null=True)  # Field name made lowercase.
    id_supervisor = models.IntegerField(db_column='ID_supervisor', blank=True, null=True)  # Field name made lowercase.
    id_admin = models.IntegerField(db_column='ID_admin', blank=True, null=True)  # Field name made lowercase.
    id_leader = models.IntegerField(db_column='ID_leader', blank=True, null=True)  # Field name made lowercase.
    id_data = models.IntegerField(db_column='ID_data', blank=True, null=True)  # Field name made lowercase.
    mail_yesno = models.IntegerField(db_column='Mail_yesno', blank=True, null=True)  # Field name made lowercase.
    id_agent_schedule = models.IntegerField(db_column='ID_agent_schedule', blank=True, null=True)  # Field name made lowercase.
    id_agent_money10 = models.IntegerField(db_column='ID_agent_money10', blank=True, null=True)  # Field name made lowercase.
    id_agent_keymember = models.IntegerField(db_column='ID_agent_keymember', blank=True, null=True)  # Field name made lowercase.
    g_account = models.CharField(db_column='G_account', max_length=32)  # Field name made lowercase.
    g_password = models.CharField(db_column='G_password', max_length=32)  # Field name made lowercase.
    fee = models.PositiveIntegerField(db_column='Fee', blank=True, null=True)  # Field name made lowercase.
    email_schedule = models.CharField(db_column='Email_schedule', max_length=32)  # Field name made lowercase.
    shoe_size = models.CharField(max_length=32, blank=True, null=True)
    line_id = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'member_team'
        unique_together = (('id', 'memberno'),)


class Menu(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=32)  # Field name made lowercase.
    level = models.CharField(db_column='Level', max_length=1)  # Field name made lowercase.
    fid = models.CharField(db_column='FID', max_length=8)  # Field name made lowercase.
    oid = models.CharField(db_column='OID', max_length=8)  # Field name made lowercase.
    link = models.CharField(db_column='Link', max_length=48)  # Field name made lowercase.
    auth = models.CharField(db_column='Auth', max_length=1)  # Field name made lowercase.
    col = models.CharField(db_column='Col', max_length=4, blank=True, null=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'menu'
        unique_together = (('id', 'name'),)


class River(models.Model):
    river_name = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    vertical_difficulty = models.CharField(max_length=10, blank=True, null=True)
    aquatic_difficulty = models.CharField(max_length=10, blank=True, null=True)
    retreat_difficulty = models.CharField(max_length=10, blank=True, null=True)
    rating = models.IntegerField(blank=True, null=True)
    canyon_coordinates = models.CharField(max_length=50, blank=True, null=True)
    exit_coordinates = models.CharField(max_length=50, blank=True, null=True)
    altitude = models.IntegerField(blank=True, null=True)
    waterfall_height = models.IntegerField(blank=True, null=True)
    number_of_waterfalls = models.IntegerField(blank=True, null=True)
    equipment_needed = models.TextField(blank=True, null=True)
    anchor_status = models.CharField(max_length=50, blank=True, null=True)
    approach_time = models.CharField(max_length=50, blank=True, null=True)
    return_time = models.CharField(max_length=50, blank=True, null=True)
    transportation = models.CharField(max_length=255, blank=True, null=True)
    rock_type = models.CharField(max_length=255, blank=True, null=True)
    best_season = models.CharField(max_length=50, blank=True, null=True)
    photos_videos_link = models.TextField(blank=True, null=True)
    plan_link = models.TextField(blank=True, null=True)
    notices = models.TextField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'river'


class RiverIndex(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    river = models.CharField(db_column='River', unique=True, max_length=16)  # Field name made lowercase.
    place_1 = models.CharField(db_column='Place_1', max_length=8)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'river_index'
        unique_together = (('id', 'river'),)


class Schedule(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    date_start = models.CharField(db_column='Date_start', max_length=10)  # Field name made lowercase.
    day = models.CharField(db_column='Day', max_length=8)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=256)  # Field name made lowercase.
    introduction = models.TextField(db_column='Introduction')  # Field name made lowercase.
    meetplace = models.CharField(db_column='Meetplace', max_length=128)  # Field name made lowercase.
    photo = models.CharField(db_column='Photo', max_length=1)  # Field name made lowercase.
    txt = models.CharField(db_column='Txt', max_length=1)  # Field name made lowercase.
    quality = models.CharField(db_column='Quality', max_length=32)  # Field name made lowercase.
    end_time = models.CharField(db_column='End_time', max_length=10)  # Field name made lowercase.
    online = models.CharField(db_column='Online', max_length=4)  # Field name made lowercase.
    note = models.CharField(db_column='Note', max_length=128)  # Field name made lowercase.
    call_memberno = models.CharField(db_column='Call_memberno', max_length=4)  # Field name made lowercase.
    sch_style = models.CharField(db_column='Sch_style', max_length=4)  # Field name made lowercase.
    sch_type1 = models.CharField(db_column='Sch_type1', max_length=4, blank=True, null=True)  # Field name made lowercase.
    sch_type2 = models.CharField(db_column='Sch_type2', max_length=4)  # Field name made lowercase.
    sch_type3 = models.CharField(db_column='Sch_type3', max_length=4)  # Field name made lowercase.
    sch_type4 = models.CharField(db_column='Sch_type4', max_length=4)  # Field name made lowercase.
    check_schedule = models.CharField(db_column='Check_schedule', max_length=8)  # Field name made lowercase.
    cancel_schedule = models.CharField(db_column='Cancel_schedule', max_length=1)  # Field name made lowercase.
    river_first = models.CharField(db_column='River_first', max_length=1)  # Field name made lowercase.
    river_source = models.CharField(db_column='River_source', max_length=1)  # Field name made lowercase.
    river_high = models.CharField(db_column='River_high', max_length=1)  # Field name made lowercase.
    river_middle = models.CharField(db_column='River_middle', max_length=1)  # Field name made lowercase.
    river_low = models.CharField(db_column='River_low', max_length=1)  # Field name made lowercase.
    river_mc = models.CharField(db_column='River_MC', max_length=1)  # Field name made lowercase.
    rockclimbing = models.CharField(db_column='Rockclimbing', max_length=1)  # Field name made lowercase.
    training = models.CharField(db_column='Training', max_length=1)  # Field name made lowercase.
    others = models.CharField(db_column='Others', max_length=1)  # Field name made lowercase.
    river_scc = models.CharField(db_column='River_scc', max_length=1)  # Field name made lowercase.
    river_sc = models.CharField(db_column='River_sc', max_length=1)  # Field name made lowercase.
    river_important = models.CharField(db_column='River_important', max_length=1)  # Field name made lowercase.
    startx = models.CharField(db_column='StartX', max_length=8)  # Field name made lowercase.
    starty = models.CharField(db_column='StartY', max_length=8)  # Field name made lowercase.
    endx = models.CharField(db_column='EndX', max_length=8)  # Field name made lowercase.
    endy = models.CharField(db_column='EndY', max_length=8)  # Field name made lowercase.
    car_name = models.CharField(db_column='Car_name', max_length=8)  # Field name made lowercase.
    car_mobile = models.CharField(db_column='Car_mobile', max_length=16)  # Field name made lowercase.
    team = models.CharField(db_column='Team', max_length=1)  # Field name made lowercase.
    t_shiayao = models.CharField(db_column='T_shiayao', max_length=1)  # Field name made lowercase.
    t_tankan = models.CharField(db_column='T_tankan', max_length=1)  # Field name made lowercase.
    t_milan = models.CharField(db_column='T_milan', max_length=1)  # Field name made lowercase.
    t_mohei = models.CharField(db_column='T_mohei', max_length=1)  # Field name made lowercase.
    t_soyuan = models.CharField(db_column='T_soyuan', max_length=1)  # Field name made lowercase.
    t_yindo = models.CharField(db_column='T_yindo', max_length=1)  # Field name made lowercase.
    t_hotspring = models.CharField(db_column='T_hotspring', max_length=1)  # Field name made lowercase.
    fee_insurance = models.CharField(db_column='Fee_insurance', max_length=8)  # Field name made lowercase.
    fee_insurance_yesno = models.CharField(db_column='Fee_insurance_yesno', max_length=1)  # Field name made lowercase.
    fee_activitiy = models.CharField(db_column='Fee_activitiy', max_length=8)  # Field name made lowercase.
    fee_activity_yesno = models.CharField(db_column='Fee_activity_yesno', max_length=1)  # Field name made lowercase.
    aftermap = models.CharField(db_column='Aftermap', max_length=128, blank=True, null=True)  # Field name made lowercase.
    plan = models.CharField(db_column='Plan', max_length=128)  # Field name made lowercase.
    event_id = models.CharField(max_length=200, blank=True, null=True)
    teamapplicationseq = models.IntegerField(db_column='teamApplicationSeq', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'schedule'

    def __str__(self) -> str:
        ret_dict: dict = {
            'id': self.id,
            'date_start': self.date_start,
            'day': self.day,
            'title': self.title,
            'introduction': self.introduction,
            'online': self.online,
        }
        return json.dumps(ret_dict, ensure_ascii=False)


class ScheduleLeader(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.
    fid = models.CharField(db_column='FID', max_length=8)  # Field name made lowercase.
    sort = models.CharField(db_column='Sort', max_length=4)  # Field name made lowercase.
    valid = models.CharField(db_column='Valid', max_length=4)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'schedule_leader'
        unique_together = (('id', 'memberno'),)


class ScheduleRecord(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fid = models.CharField(db_column='FID', max_length=8)  # Field name made lowercase.
    memberno = models.CharField(db_column='Memberno', max_length=8)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'schedule_record'


class Talk(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    hit = models.CharField(db_column='Hit', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    reply = models.CharField(db_column='Reply', max_length=64, db_collation='big5_chinese_ci')  # Field name made lowercase.
    lasttime = models.DateTimeField(db_column='Lasttime')  # Field name made lowercase.
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.
    section = models.CharField(db_column='Section', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'talk'


class Talk2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    hit = models.CharField(db_column='Hit', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    reply = models.CharField(db_column='Reply', max_length=64, db_collation='big5_chinese_ci')  # Field name made lowercase.
    lasttime = models.DateTimeField(db_column='Lasttime')  # Field name made lowercase.
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.
    section = models.CharField(db_column='Section', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'talk2'


class Talk4(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=128, db_collation='utf8_general_ci')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64, db_collation='utf8_general_ci')  # Field name made lowercase.
    content = models.TextField(db_column='Content', db_collation='utf8_general_ci')  # Field name made lowercase.
    http = models.CharField(db_column='Http', max_length=256, db_collation='big5_chinese_ci')  # Field name made lowercase.
    firsttime = models.DateTimeField(db_column='Firsttime')  # Field name made lowercase.
    replyid = models.CharField(db_column='ReplyID', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    who = models.CharField(db_column='Who', max_length=16, db_collation='utf8_general_ci')  # Field name made lowercase.
    remote_host = models.CharField(db_column='Remote_host', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.
    hit = models.CharField(db_column='Hit', max_length=4, db_collation='big5_chinese_ci')  # Field name made lowercase.
    reply = models.CharField(db_column='Reply', max_length=64, db_collation='big5_chinese_ci')  # Field name made lowercase.
    lasttime = models.DateTimeField(db_column='Lasttime')  # Field name made lowercase.
    top = models.CharField(db_column='Top', max_length=8, db_collation='big5_chinese_ci')  # Field name made lowercase.
    section = models.CharField(db_column='Section', max_length=16, db_collation='big5_chinese_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'talk4'
