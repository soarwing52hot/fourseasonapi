from django import http
from django.http import HttpResponse
from rest_framework.decorators import api_view

from legacy.LegacyScheduleService import get_all_schedules, get_schedule_dto_by_id, get_all_schedules_by_leader_id, \
    get_all_schedules_by_member_id
from legacy.dto import LegacyScheduleListDto, LegacyScheduleDto


def legacy_schedule_view_by_id(request: http.HttpRequest, schedule_id: int):
    schedule_dto: LegacyScheduleDto = get_schedule_dto_by_id(schedule_id)
    return HttpResponse(schedule_dto)


def masking_legacy_schedule_list_dto(schedule_list_dto: LegacyScheduleListDto) -> LegacyScheduleListDto:
    for schedule_dto in schedule_list_dto.schedule_dto_list:
        for member in schedule_dto.leader_list:
            member.masking(['email', 'mobile', 'name', 'license', 'sex', 'email'])
        for member in schedule_dto.member_list:
            member.masking(['email', 'mobile', 'name', 'license', 'sex', 'email', 'id', 'memberno'])
    return schedule_list_dto


@api_view(['GET'])
def legacy_schedule_paging_view(request):
    size: int = request.GET.get('size', 10)
    page: int = request.GET.get('page', 1)

    leader_id: str = request.GET.get('leader_id', None)
    member_id: str = request.GET.get('member_id', None)

    if leader_id is not None and member_id is not None:
        return HttpResponse(status=400)
    elif leader_id is None and member_id is None:
        schedule_list_dto: LegacyScheduleListDto = get_all_schedules(size, page)
    elif leader_id is not None:
        schedule_list_dto: LegacyScheduleListDto = get_all_schedules_by_leader_id(size, page, leader_id)
    elif member_id is not None:
        schedule_list_dto: LegacyScheduleListDto = get_all_schedules_by_member_id(size, page, member_id)
    else:
        return HttpResponse(status=400)

    if request.user.is_authenticated:
        return HttpResponse(schedule_list_dto.__str__())
    else:
        mask_schedule_list_dto: LegacyScheduleListDto = masking_legacy_schedule_list_dto(schedule_list_dto)
        return HttpResponse(mask_schedule_list_dto.__str__())
