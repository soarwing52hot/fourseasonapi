from rest_framework import serializers

from announce.models import Announcement
from user.serializers import UserDtoSerializer


class AnnouncementSerializer(serializers.ModelSerializer):
    writer = UserDtoSerializer(read_only=True)

    class Meta:
        model = Announcement
        fields = "__all__"

    def create(self, validated_data):
        return Announcement.objects.create(**validated_data)


class EmailSerializer(serializers.Serializer):
    includeUnpaid = serializers.BooleanField()
    subject = serializers.CharField()
    message = serializers.CharField()
