from django.urls import include, path
from rest_framework.routers import DefaultRouter

from announce import views

router = DefaultRouter()
router.register(r"", views.AnnouncementViewSet)

urlpatterns = [
    path("info/", include(router.urls)),
    path("options/types/", views.AnnouncementTypesAPIView.as_view(), name="announce_types"),
    path("options/availableTypes/", views.AvailableAnnouncementTypesAPIView.as_view(), name="announce_types_available"),
    path("sendEmail/", views.EmailView.as_view(), name="send-email"),
]
