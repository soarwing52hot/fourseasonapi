from rest_framework import permissions

from announce.models import AnnouncementTypes
from user.models import GroupOptions


class IsRoleOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        announce_type = request.data.get("announce_type")
        if request.method in permissions.SAFE_METHODS:
            return True

        if announce_type == AnnouncementTypes.Asscociate and not request.user.groups.filter(name=GroupOptions.Secretary).exists():
            return False

        if announce_type == AnnouncementTypes.Leader and not request.user.groups.filter(name=GroupOptions.Leader).exists():
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        if not request.user.is_authenticated:
            return False
        if request.method in ["PATCH", "PUT"]:
            if request.user.membership_number != obj.writer.membership_number:
                return False
        return True
