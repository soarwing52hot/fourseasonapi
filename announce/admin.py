from django.contrib import admin

from announce.models import Announcement


class AnnouncementAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "announce_type",
        "title",
    )
    readonly_fields = ("creation_date", "modify_date")


admin.site.register(Announcement, AnnouncementAdmin)
