from django.conf import settings
from django.core.mail import send_mail
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import filters, permissions, status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from announce import models
from announce import permissions as announce_permission
from announce import serializers
from announce.models import Announcement
from common.serializers import OptionSelectSerialzer, ResultOutputSerializer
from user.models import CustomUser, GroupOptions
from user.permissions import IsSecretaryOrReadOnly


class AnnouncementViewSet(viewsets.ModelViewSet):
    queryset = Announcement.objects.all().order_by("-id")
    serializer_class = serializers.AnnouncementSerializer
    permission_classes = [
        announce_permission.IsRoleOrReadOnly,
    ]

    search_fields = ["creation_date", "announce_type"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filterset_fields = {"creation_date": ["gte"], "announce_type": ["iexact"]}

    def perform_create(self, serializer):
        serializer.save(writer=self.request.user)


class AnnouncementTypesAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        options = models.AnnouncementTypes.choices
        serializer = OptionSelectSerialzer(options, many=True)
        return Response(serializer.data)


class AvailableAnnouncementTypesAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        groups = list(self.request.user.groups.values_list("name", flat=True))
        available_options = []
        if len(groups) > 0:
            available_options.append(str(models.AnnouncementTypes.General))
        for group in groups:
            if group == GroupOptions.Secretary:
                available_options.append(str(models.AnnouncementTypes.Asscociate))
            if group == GroupOptions.Leader:
                available_options.append(str(models.AnnouncementTypes.Leader))
        options = models.AnnouncementTypes.choices

        final_options = []
        for o in options:
            if o[0] in available_options:
                final_options.append(o)
        serializer = OptionSelectSerialzer(final_options, many=True)
        return Response(serializer.data)


class EmailView(APIView):
    permission_classes = [IsSecretaryOrReadOnly]

    @swagger_auto_schema(request_body=serializers.EmailSerializer, responses={200: ResultOutputSerializer})
    def post(self, request, format=None):
        serializer = serializers.EmailSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if serializer.validated_data is None and not serializer.validated_data:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        users = CustomUser.objects.all()
        include_none_paid = serializer.validated_data["includeUnpaid"]

        if not include_none_paid:
            users = users.filter(paid_this_year=True)

        recipient_list = list(users.values_list("email", flat=True))

        subject = serializer.validated_data["subject"]
        message = serializer.validated_data["message"]
        from_email = settings.EMAIL_HOST_USER

        subject = f"四季公用信箱通知信(請勿回信) - {subject}"
        send_mail(subject, message, from_email, recipient_list)
        output_serializer = ResultOutputSerializer({"message": "Email sent", "success": True})
        return Response(output_serializer.data, status=status.HTTP_200_OK)
