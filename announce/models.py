from django.conf import settings
from django.db import models

from common.models import BaseModel


class AnnouncementTypes(models.TextChoices):
    Asscociate = "A", "會務"
    Leader = "L", "嚮導"
    General = "G", "一般"


class Announcement(BaseModel):
    class Meta:
        db_table = "announcement"
        ordering = ["id"]

    title = models.CharField(max_length=50)
    content = models.TextField()
    announce_type = models.CharField(max_length=5, choices=AnnouncementTypes.choices)
    writer = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="announcements", on_delete=models.PROTECT)
