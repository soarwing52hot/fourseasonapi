import uuid

from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404, render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions, viewsets
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from common import utils
from user import models
from user import permissions as UserPermission
from user import serializers


class CustomUserViewSet(viewsets.ModelViewSet):
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.EditUserSerializer
    permission_classes = [UserPermission.SelfOnly]
    search_fields = ["membership_number", "username__icontains"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    http_method_names = ["get", "patch"]
    filterset_fields = {
        "membership_number": ["exact"],
        "username": ["icontains"],
    }


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [UserPermission.IsSecretaryOrReadOnly]
    http_method_names = ["get"]


class CreateUserView(generics.CreateAPIView):
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.CreateUserSerializer
    permission_classes = [UserPermission.IsFinance]


class ChangePasswordView(generics.UpdateAPIView):
    queryset = models.CustomUser.objects.all()
    permission_classes = (UserPermission.SelfOnly,)
    serializer_class = serializers.ChangePasswordSerializer
    http_method_names = ["put"]


class GetUserGroupView(generics.ListAPIView):
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserWithGroupSerializer
    permission_classes = [UserPermission.IsSecretaryOrReadOnly]

    def get_queryset(self):
        keyword = self.kwargs.get("keyword")

        if keyword == "all":
            return models.CustomUser.objects.order_by("membership_number").all()

        users = models.CustomUser.objects.filter(paid_this_year=True)
        if keyword == "paid":
            return users.order_by("membership_number").all()

        if keyword == "leader":
            leader_group = Group.objects.get(name="Leader")
            leaders = models.CustomUser.objects.filter(groups=leader_group)
            return leaders.order_by("membership_number").all()
        try:
            membership_number = int(keyword)
            users = users.filter(membership_number=membership_number)
        except ValueError:
            users = models.CustomUser.objects.filter(username__contains=keyword)

        return users.order_by("membership_number").all()


class ChangeGroupView(generics.UpdateAPIView):
    queryset = models.CustomUser.objects.all()
    permission_classes = (UserPermission.IsSecretaryOrReadOnly,)
    serializer_class = serializers.ChangeGroupSerializer
    http_method_names = ["put"]

    def get_object(self):
        membership_number = self.request.data.get("membership_number")
        return get_object_or_404(models.CustomUser, membership_number=membership_number)

    def update(self, request, *args, **kwargs):
        group_names = request.data.get("groups")
        user = self.get_object()
        groups = Group.objects.all()

        for group in groups:
            if group.name in group_names:
                user.groups.add(group)
            else:
                user.groups.remove(group)

        return Response("success")


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.ProfilePhotoSerializer
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    http_method_names = ["get", "post"]

    def perform_create(self, serializer):
        user: models.CustomUser = self.request.user
        image = self.request.data.get("image_url")

        image_uuid = str(uuid.uuid4())
        blob_name = f"profile/{image_uuid}"

        serializer.validated_data["image_url"] = utils.upload_image_to_gcp(blob_name, image)

        user.profile_uuid = image_uuid
        user.save()
