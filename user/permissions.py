from rest_framework import permissions

from user.models import GroupOptions


class IsSecretaryOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in permissions.SAFE_METHODS:
            return True

        if GroupOptions.Secretary in request.user.groups.values_list("name", flat=True):
            return True


class IsFinance(permissions.BasePermission):
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if GroupOptions.Finance in request.user.groups.values_list("name", flat=True):
            return True


class IsFinanceOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in permissions.SAFE_METHODS:
            return True

        if GroupOptions.Finance in request.user.groups.values_list("name", flat=True):
            return True


class SelfOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # return request.user.groups.filter(name="Leader").exists()
        return True

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        # print(obj.membership_number, request.user.membership_number)
        return request.user.membership_number == obj.membership_number
