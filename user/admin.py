from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm

    model = CustomUser
    readonly_fields = ("membership_number",)

    list_display = (
        "membership_number",
        "username",
        "email",
        "is_active",
        "is_staff",
        "is_superuser",
        "last_login",
    )
    list_filter = ("is_active", "is_staff", "is_superuser")
    fieldsets = (
        (
            "基礎資料",
            {
                "fields": (
                    "membership_number",
                    "username",
                    "email",
                    "password",
                    "gender",
                    "birth_date",
                    "birth_location",
                    "citizen_id",
                    "phone",
                    "land_line",
                    "postal_code",
                    "address",
                    "emergency_contact_name",
                    "emergency_contact_phone",
                    "emergency_contact_relation",
                    "facebook_name",
                    "paid_this_year",
                )
            },
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_staff",
                    "is_active",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        ("Dates", {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "email",
                    "citizen_id",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                ),
            },
        ),
    )
    search_fields = ("membership_number",)
    ordering = ("membership_number",)


admin.site.register(CustomUser, CustomUserAdmin)
