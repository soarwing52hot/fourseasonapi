from django.contrib.auth.models import Group
from rest_framework import serializers

from user.models import CustomUser


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            "membership_number",
            "username",
            "email",
            "password",
            "gender",
            "birth_date",
            "birth_location",
            "citizen_id",
            "phone",
            "land_line",
            "postal_code",
            "address",
            "emergency_contact_name",
            "emergency_contact_phone",
            "emergency_contact_relation",
            "facebook_name",
            "paid_this_year",
        )


class ChangeGroupSerializer(serializers.ModelSerializer):
    membership_number = serializers.IntegerField()
    groups = serializers.ListSerializer(child=serializers.CharField())

    class Meta:
        model = CustomUser
        fields = ("membership_number", "groups")


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("id", "name")


class UserWithGroupSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(read_only=True, many=True)

    class Meta:
        model = CustomUser
        fields = ("membership_number", "username", "groups")


class ProfilePhotoSerializer(serializers.ModelSerializer):
    image_url = serializers.ImageField(required=False)

    class Meta:
        model = CustomUser
        fields = ["membership_number", "image_url"]


class EditUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        depth = 1
        fields = (
            "membership_number",
            "username",
            "email",
            "gender",
            "birth_date",
            "birth_location",
            "citizen_id",
            "phone",
            "land_line",
            "postal_code",
            "address",
            "emergency_contact_name",
            "emergency_contact_phone",
            "emergency_contact_relation",
            "facebook_name",
            "profile_uuid",
            "paid_this_year",
        )


class UserDtoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            "membership_number",
            "username",
            "email",
            "paid_this_year",
        )


class UserNameBlockedSerializer(UserDtoSerializer):
    username = serializers.SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = (
            "membership_number",
            "username",
        )

    def get_username(self, obj):
        return f"{obj.username[0]}OO"


class UserDetailDtoSerializer(serializers.ModelSerializer):
    gender_display = serializers.CharField(source="get_gender_display")

    class Meta:
        model = CustomUser
        fields = (
            "membership_number",
            "username",
            "email",
            "gender",
            "gender_display",
            "birth_date",
            "birth_location",
            "citizen_id",
            "phone",
            "land_line",
            "address",
            "emergency_contact_name",
            "emergency_contact_phone",
            "emergency_contact_relation",
            "paid_this_year",
        )


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    password2 = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = CustomUser
        fields = ("old_password", "password", "password2")

    def validate(self, attrs):
        if attrs["password"] != attrs["password2"]:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def validate_old_password(self, value):
        user = self.context["request"].user
        if not user.check_password(value):
            raise serializers.ValidationError({"old_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):
        instance.set_password(validated_data["password"])
        instance.save()

        return instance
