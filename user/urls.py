from django.urls import include, path
from rest_framework.routers import DefaultRouter

from user import views

router = DefaultRouter()
router.register(r"CustomUser", views.CustomUserViewSet)
router.register(r"ProfilePhoto", views.ProfileViewSet)
router.register(r"group", views.GroupViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("create/", views.CreateUserView.as_view()),
    path("change_password/<int:pk>/", views.ChangePasswordView.as_view(), name="auth_change_password"),
    path("changeGroup/", views.ChangeGroupView.as_view(), name="auth_change_group"),
    path("getGroups/<str:keyword>", views.GetUserGroupView.as_view(), name="auth_get_groups"),
]
