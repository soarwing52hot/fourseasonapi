from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from PIL import Image

from common.models import GENDER_CHOICES

from .managers import CustomUserManager


class GroupOptions:
    Secretary = "Secretary"
    Leader = "Leader"
    Finance = "Finance"


class LegacyUser(models.Model):
    memberNo = models.CharField(max_length=8)
    Name = models.CharField(max_length=50)
    License = models.CharField(max_length=8)
    Sex = models.CharField(max_length=10)
    Bd = models.CharField(max_length=12)
    ID_no = models.CharField(max_length=16)
    E_contact = models.CharField(max_length=8)
    E_relationship = models.CharField(max_length=16)
    E_phone = models.CharField(max_length=32)
    Phone_home = models.CharField(max_length=32)
    Mobile = models.CharField(max_length=12)
    Email = models.EmailField(max_length=40)
    Msn = models.CharField(max_length=40)
    Address = models.TextField()
    Zipcode = models.CharField(max_length=8)
    Join_year = models.CharField(max_length=4)
    Fee = models.IntegerField()
    Password = models.CharField(max_length=32)

    class Meta:
        db_table = "legacy_member"
        managed = False


class CustomUser(AbstractUser):
    username = models.CharField(max_length=30, unique=False)
    membership_number = models.AutoField(primary_key=True)
    email = models.EmailField(_("email address"), blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True)
    birth_date = models.DateField(default=timezone.now)
    birth_location = models.CharField(max_length=20, null=True, blank=True)
    citizen_id = models.CharField(max_length=20, blank=True, unique=True)
    phone = models.CharField(max_length=20, blank=True)
    land_line = models.CharField(max_length=20, null=True, blank=True)
    postal_code = models.CharField(max_length=5, blank=True)
    address = models.CharField(max_length=300, blank=True)
    emergency_contact_name = models.CharField(max_length=20, blank=True)
    emergency_contact_phone = models.CharField(max_length=30, blank=True)
    emergency_contact_relation = models.CharField(max_length=10, blank=True)
    license = models.CharField(max_length=8, blank=True)
    paid_this_year = models.BooleanField(default=False)
    profile_uuid = models.CharField(max_length=36, blank=True)

    facebook_name = models.CharField(max_length=100, blank=True)

    USERNAME_FIELD = "membership_number"
    REQUIRED_FIELDS = ["email"]

    objects = CustomUserManager()

    def __str__(self):
        return self.username

    def is_associate(self):
        return self.groups.exists()
