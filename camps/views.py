import os
import uuid

import openpyxl
from django.contrib.auth import get_user_model
from django.http import FileResponse
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404, render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions, status, viewsets
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView

from camps import models, serializers
from camps.models import Brochure, Camp, CampParticipant, CampPayment
from camps.permissions import CampPermission, IsOwnerOrLeader, PaymentPermission
from common import utils
from common.serializers import OptionSelectSerialzer
from user.models import CustomUser
from user.permissions import IsSecretaryOrReadOnly
from user.serializers import UserDtoSerializer

User = get_user_model()


class CampViewSet(viewsets.ModelViewSet):
    queryset = Camp.objects.all()
    serializer_class = serializers.CampSerializer
    permission_classes = [CampPermission]

    search_fields = ["trip_date_start", "trip_date_end"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filterset_fields = {"trip_date_start": ["gte"], "trip_date_end": ["lte"]}

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        membership_number = self.request.data["owner"]
        owner = get_object_or_404(CustomUser, membership_number=membership_number)
        serializer.save(owner=owner)

    def partial_update(self, request, *args, **kwargs):
        membership_number = self.request.data["owner"]
        owner = get_object_or_404(CustomUser, membership_number=membership_number)
        request.data["owner"] = owner
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save(owner=owner)

        return Response(serializer.data)


class CampParticipantViewSet(viewsets.ModelViewSet):
    queryset = CampParticipant.objects.all().select_related("camppayment").order_by("pk")
    serializer_class = serializers.CampParticipantSerializer
    permission_classes = [permissions.AllowAny]
    search_fields = ["camp__id", "citizen_id"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filterset_fields = {"camp__id": ["iexact"], "citizen_id": ["iexact"]}

    def get_serializer_class(self):
        if self.request.method == "POST":
            return serializers.CampParticipantSerializer

        else:
            return serializers.CampParticipantViewSerializer

    def create(self, request, *args, **kwargs):
        camp_id = request.data.get("camp")
        camp = get_object_or_404(Camp, id=camp_id)
        if not camp.register_available:
            raise PermissionDenied("Registration is not available for this camp.")
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CampPaymentViewSet(viewsets.ModelViewSet):
    queryset = CampPayment.objects.all()
    serializer_class = serializers.CampPaymentSerializer
    permission_classes = [PaymentPermission]


class StaffViewSet(viewsets.ModelViewSet):
    queryset = Camp.objects.all()
    serializer_class = serializers.StaffSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    http_method_names = ["get", "patch"]

    def retrieve(self, request, pk=None):
        camp_id = pk
        model = get_user_model()
        staff = model.objects.filter(staff__id=camp_id)
        serializer = UserDtoSerializer(staff, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = request.data
        member_id = data["membership_number"]

        model = get_user_model()
        try:
            staff = model.objects.get(membership_number=member_id)
        except model.DoesNotExist:
            return Response(False)

        join_staff = data["join"]

        camp = Camp.objects.get(id=pk)
        if camp.can_change_staff is False:
            return Response(False)

        if join_staff:
            camp.staff.add(member_id)
        else:
            camp.staff.remove(member_id)
        camp.save()
        return Response(True)


class InsuranceView(generics.RetrieveAPIView):
    queryset = Camp.objects.prefetch_related("campparticipant_set").all()
    serializer_class = serializers.CampSerializer
    permission_classes = [IsOwnerOrLeader]

    def retrieve(self, request, pk):
        wb_obj = openpyxl.Workbook()
        sheet = wb_obj.active
        camp = self.get_object()
        staff_list = [camp.owner] + list(camp.leaders.all()) + list(camp.staff.all())

        participant_list = list(camp.campparticipant_set.all())
        for index, user in enumerate(staff_list, start=1):
            sheet[f"A{index}"] = user.username
            sheet[f"B{index}"] = user.citizen_id
            sheet[f"C{index}"] = user.birth_date
            sheet[f"D{index}"] = 300

        for index, user in enumerate(participant_list, start=len(staff_list) + 1):
            sheet[f"A{index}"] = user.name
            sheet[f"B{index}"] = user.citizen_id
            sheet[f"C{index}"] = user.birth_date
            sheet[f"D{index}"] = 300

        temp_file_name = f"insurance_{pk}.xlsx"  # 要設計清理資料機制
        temp_file_full_path = f"camps/{temp_file_name}"
        wb_obj.save(temp_file_full_path)
        document = open(temp_file_full_path, "rb")
        response = HttpResponse(document, content_type="application/msexcel")
        response["Content-Disposition"] = f"attachment; filename={temp_file_name}"
        os.remove(temp_file_full_path)
        return response


class StaffExcelView(generics.RetrieveAPIView):
    queryset = Camp.objects.prefetch_related("campparticipant_set").all()
    serializer_class = serializers.CampSerializer
    permission_classes = [IsOwnerOrLeader]

    def retrieve(self, request, pk):
        wb_obj = openpyxl.Workbook()
        sheet = wb_obj.active
        camp: models.Camp = self.get_object()
        staff_list = [camp.owner] + list(camp.leaders.all()) + list(camp.staff.all())
        headers = (
            ("A1", "會員編號"),
            ("B1", "姓名"),
            ("C1", "性別"),
            ("D1", "出生日期"),
            ("E1", "身分證字號"),
            ("F1", "電話"),
            ("G1", "電子郵件"),
            ("H1", "地址"),
            ("I1", "緊急聯絡人姓名"),
            ("J1", "緊急聯絡人電話"),
            ("K1", "緊急聯絡人關係"),
        )
        for col, val in headers:
            sheet[col] = val

        for index, user in enumerate(staff_list, start=2):
            sheet[f"A{index}"] = user.membership_number
            sheet[f"B{index}"] = user.username
            sheet[f"C{index}"] = user.get_gender_display()
            sheet[f"D{index}"] = user.birth_date
            sheet[f"E{index}"] = user.citizen_id
            sheet[f"F{index}"] = user.phone
            sheet[f"G{index}"] = user.email
            sheet[f"H{index}"] = user.address
            sheet[f"I{index}"] = user.emergency_contact_name
            sheet[f"J{index}"] = user.emergency_contact_phone
            sheet[f"K{index}"] = user.emergency_contact_relation

        temp_file_name = f"namelist_{pk}.xlsx"
        temp_file_full_path = f"camps/{temp_file_name}"
        wb_obj.save(temp_file_full_path)
        document = open(temp_file_full_path, "rb")
        response = HttpResponse(document, content_type="application/msexcel")
        response["Content-Disposition"] = f"attachment; filename={temp_file_name}"
        os.remove(temp_file_full_path)
        return response


class CampParticipantExcelView(generics.RetrieveAPIView):
    queryset = Camp.objects.prefetch_related("campparticipant_set").all()
    serializer_class = serializers.CampSerializer
    permission_classes = [IsOwnerOrLeader]

    def retrieve(self, request, pk):
        wb_obj = openpyxl.Workbook()
        sheet = wb_obj.active
        camp: models.Camp = self.get_object()
        staff_list = list(camp.campparticipant_set.all())
        headers = (
            ("A1", "編號"),
            ("B1", "姓名"),
            ("C1", "性別"),
            ("D1", "出生日期"),
            ("E1", "身分證字號"),
            ("F1", "電話"),
            ("G1", "電子郵件"),
            ("H1", "地址"),
            ("I1", "緊急聯絡人姓名"),
            ("J1", "緊急聯絡人電話"),
            ("K1", "緊急聯絡人關係"),
        )
        for col, val in headers:
            sheet[col] = val

        for index, user in enumerate(staff_list, start=2):
            sheet[f"A{index}"] = user.id
            sheet[f"B{index}"] = user.name
            sheet[f"C{index}"] = user.get_gender_display()
            sheet[f"D{index}"] = user.birth_date
            sheet[f"E{index}"] = user.citizen_id
            sheet[f"F{index}"] = user.phone
            sheet[f"G{index}"] = user.email
            sheet[f"H{index}"] = user.address
            sheet[f"I{index}"] = user.emergency_contact_name
            sheet[f"J{index}"] = user.emergency_contact_phone
            sheet[f"K{index}"] = user.emergency_contact_relation

        temp_file_name = f"namelist_{pk}.xlsx"
        temp_file_full_path = f"camps/{temp_file_name}"
        wb_obj.save(temp_file_full_path)
        document = open(temp_file_full_path, "rb")
        response = HttpResponse(document, content_type="application/msexcel")
        response["Content-Disposition"] = f"attachment; filename={temp_file_name}"
        os.remove(temp_file_full_path)
        return response


class CampRecordSearchView(generics.ListAPIView):
    def list(self, request, membership_number, *args, **kwargs):
        serializer_class = serializers.CampSerializer
        try:
            user: CustomUser = User.objects.get(membership_number=membership_number)
            personal_id = user.citizen_id
        except User.DoesNotExist:
            return Response({"error": "User not found"}, status=status.HTTP_404_NOT_FOUND)

        camps = Camp.objects.filter(campparticipant__citizen_id=personal_id)
        serializer = serializers.CampSerializer(camps, many=True)

        if camps.exists():
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response([], status=status.HTTP_200_OK)


class CampBrochureUploadView(generics.CreateAPIView):
    permission_classes = [IsSecretaryOrReadOnly]
    queryset = Brochure.objects.all()
    serializer_class = serializers.CampBrochureUploadSerializer

    def perform_create(self, serializer):
        uploaded_file = self.request.data.get("pdf_uuid")

        file_uuid = str(uuid.uuid4())
        blob_name = f"brochure/{file_uuid}"

        utils.upload_pdf_to_gcp(blob_name, uploaded_file)

        serializer.save(pdf_uuid=file_uuid)


class CampBrochureDownloadView(generics.RetrieveAPIView):
    queryset = Brochure.objects.all()
    permission_classes = [permissions.AllowAny]
    serializer_class = serializers.CampBrochureSerializer

    def get_object(self):
        camp_type = self.kwargs.get("camp_type")
        brochure = Brochure.objects.filter(camp_type=camp_type).order_by("-created_on").first()

        if brochure is None:
            raise NotFound("No files found, please upload first!")
        return brochure


class CampTypeOptionsAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        camp_types = models.CampTypes.choices
        serializer = OptionSelectSerialzer(camp_types, many=True)
        return Response(serializer.data)
