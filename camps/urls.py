from django.urls import include, path
from rest_framework.routers import DefaultRouter

from camps import views

router = DefaultRouter()
router.register(r"info", views.CampViewSet)
router.register(r"participant", views.CampParticipantViewSet)
router.register(r"payment", views.CampPaymentViewSet)
router.register(r"staff", views.StaffViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("insurance/<int:pk>", views.InsuranceView.as_view()),
    path("record/<int:membership_number>/", views.CampRecordSearchView.as_view()),
    path("brochure/", views.CampBrochureUploadView.as_view(), name="pdf-upload"),
    path("brochure/<str:camp_type>/", views.CampBrochureDownloadView.as_view(), name="pdf-download"),
    path("types/", views.CampTypeOptionsAPIView.as_view(), name="camp_types"),
    path("staffExcel/<int:pk>", views.StaffExcelView.as_view()),
    path("participantExcel/<int:pk>", views.CampParticipantExcelView.as_view()),
]
