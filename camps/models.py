from datetime import date

from django.conf import settings
from django.db import models
from django.utils import timezone

from common.models import GENDER_CHOICES, PaymentBaseModel
from user.models import CustomUser


class CampTypes(models.TextChoices):
    Experience = "E", "體驗營"
    Basic = "B", "初級溯溪營"
    Advanced = "Adv", "進階營"
    Guide = "Guide", "嚮導營"


class Camp(models.Model):
    class Meta:
        db_table = "camps"
        ordering = ["id"]

    title = models.CharField(max_length=50)
    content = models.CharField(max_length=50, blank=True)
    # 總召
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="camps", on_delete=models.PROTECT
    )
    # 領隊
    leaders = models.ManyToManyField(CustomUser, related_name="CampLeaders", blank=True)
    # 工作人員
    staff = models.ManyToManyField(CustomUser, related_name="staff", blank=True)
    can_change_staff = models.BooleanField(default=False)
    announcement = models.IntegerField(blank=True, null=True)

    register_date_start = models.DateTimeField(default=timezone.now)
    register_date_due = models.DateTimeField(default=timezone.now, blank=True)

    trip_date_start = models.DateField(default=timezone.now, null=True, blank=True)
    trip_date_end = models.DateField(default=timezone.now, null=True, blank=True)

    participant_limit = models.IntegerField(default=0)

    camp_type = models.CharField(max_length=5, choices=CampTypes.choices)

    def __str__(self):
        return self.title

    def participant_full(self):
        if not self.participant_limit:
            return False
        total = [i for i in self.participants.all()]
        return total.count > self.participant_limit

    @property
    def register_available(self):
        if timezone.now() < self.register_date_start:
            return False
        if timezone.now() > self.register_date_due:
            return False

        if self.participant_limit == 0:
            return True

        if self.campparticipant_set.count() >= self.participant_limit:
            return False

        return True


class CampParticipant(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=10)
    camp = models.ForeignKey(Camp, on_delete=models.CASCADE)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    birth_date = models.DateField(default=timezone.now)
    birth_location = models.CharField(max_length=20, null=True, blank=True)
    citizen_id = models.CharField(max_length=10)
    phone = models.CharField(max_length=10)
    land_line = models.CharField(max_length=10, null=True, blank=True)
    email = models.EmailField()
    postal_code = models.CharField(max_length=5)
    address = models.CharField(max_length=300)
    emergency_contact_name = models.CharField(max_length=10)
    emergency_contact_phone = models.CharField(max_length=10)
    emergency_contact_relation = models.CharField(max_length=10)

    facebook_name = models.CharField(max_length=50, blank=True, default="")
    line_id = models.CharField(max_length=50)

    experience = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class CampPayment(PaymentBaseModel):
    class Meta:
        db_table = "camp_payment"

    camp_participant = models.OneToOneField(
        CampParticipant, on_delete=models.CASCADE, primary_key=True, default=""
    )
    confirm_person = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="payment_camp",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )


class Brochure(models.Model):
    pdf_uuid = models.CharField(max_length=36, blank=True)
    camp_type = models.CharField(max_length=5, choices=CampTypes.choices)
    created_on = models.DateField(default=date.today)
