from rest_framework import permissions

from user.models import GroupOptions


class PaymentPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if view.action in ["create", "retrieve", "list"]:
            return True
        if view.action in ["update", "partial_update"]:
            return request.user.groups.filter(name=GroupOptions.Finance).exists()

    def has_object_permission(self, request, view, obj):
        if view.action in ["create", "retrieve", "list"]:
            return True
        if not request.user.is_authenticated:
            return False

        return request.user.groups.filter(name=GroupOptions.Finance).exists()


class CampPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        # 到每個營隊單獨判斷
        if view.action in ["update", "partial_update"]:
            return True

        # 只有秘書長可以建立營隊
        if view.action == "create":
            if GroupOptions.Secretary in request.user.groups.values_list("name", flat=True):
                return True
            return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        if view.action in ["update", "partial_update"]:
            if request.user.membership_number == obj.owner.membership_number:
                return True


class IsOwnerOrLeader(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.membership_number == obj.owner.membership_number:
            return True
        leader_ids = [leader.membership_number for leader in obj.leaders.all()]
        if request.user.membership_number in leader_ids:
            return True
