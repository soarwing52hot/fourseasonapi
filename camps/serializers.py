from django.contrib.auth import get_user_model
from rest_framework import serializers

from camps import models
from camps.models import Brochure, Camp, CampParticipant, CampPayment
from user.serializers import UserDtoSerializer

USER = get_user_model()


class StaffSerializer(serializers.ModelSerializer):
    membership_number = serializers.IntegerField(required=False)
    join = serializers.BooleanField(default=False)

    class Meta:
        model = Camp
        fields = ["membership_number", "join"]


class CampSerializer(serializers.ModelSerializer):
    register_available = serializers.ReadOnlyField()
    owner = UserDtoSerializer(read_only=True)
    leaders = serializers.PrimaryKeyRelatedField(many=True, queryset=USER.objects.all(), allow_null=True, required=False)
    staff = UserDtoSerializer(read_only=True, many=True)
    camp_type_name = serializers.SerializerMethodField()

    class Meta:
        model = Camp
        fields = "__all__"

    def get_camp_type_name(self, obj):
        return obj.get_camp_type_display()

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["leaders"] = UserDtoSerializer(instance.leaders.all(), many=True).data
        return representation

    def update(self, instance, validated_data):
        leaders_data = validated_data.pop("leaders", None)
        if leaders_data is not None:
            instance.leaders.set(leaders_data)
        return super().update(instance, validated_data)


class CampPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = CampPayment

    def create(self, validated_data):
        validated_data["confirmed"] = False
        validated_data["confirm_person"] = None

        return CampPayment.objects.create(**validated_data)


class CampParticipantSerializer(serializers.ModelSerializer):
    payment = CampPaymentSerializer(source="camppayment", required=False)

    class Meta:
        model = CampParticipant
        fields = "__all__"

    def create(self, validated_data):
        return CampParticipant.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.phone = validated_data.get("phone", instance.code)
        instance.save()
        return instance


class CampParticipantViewSerializer(serializers.ModelSerializer):
    payment = CampPaymentSerializer(source="camppayment", required=False)
    citizen_id = serializers.SerializerMethodField()

    class Meta:
        model = CampParticipant
        fields = ("id", "name", "birth_date", "citizen_id", "payment")

    def get_citizen_id(self, obj):
        # 把身分證字號碼掉
        return str(obj.citizen_id)[-4:]


class CampRecordSerializer(serializers.Serializer):
    memebership_number = serializers.CharField(max_length=200)


class CampBrochureUploadSerializer(serializers.ModelSerializer):
    pdf_uuid = serializers.FileField(allow_empty_file=False, required=True)

    class Meta:
        model = Brochure
        fields = ("camp_type", "pdf_uuid")


class CampBrochureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brochure
        fields = ("camp_type", "pdf_uuid")


class CampTypeSerialzer(serializers.Serializer):
    value = serializers.SerializerMethodField()
    label = serializers.SerializerMethodField()

    def get_value(self, obj):
        return obj[0]

    def get_label(self, obj):
        return obj[1]
