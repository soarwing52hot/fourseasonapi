from django.contrib import admin

from camps.models import Brochure, Camp, CampParticipant, CampPayment


class CampAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
        "content",
        "trip_date_start",
        "trip_date_end",
        "owner",
    )


class CampParticipantAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "facebook_name",
        "camp",
        "gender",
        "birth_date",
        "birth_location",
        "citizen_id",
        "phone",
        "land_line",
        "email",
        "postal_code",
        "address",
        "experience",
    )


class CampPaymentAdmin(admin.ModelAdmin):
    list_display = (
        "camp_participant",
        "confirmed",
        "account_number",
        "transfer_date",
        "memo",
    )


admin.site.register(Camp, CampAdmin)
admin.site.register(CampParticipant, CampParticipantAdmin)
admin.site.register(CampPayment, CampPaymentAdmin)


class BrochureAdmin(admin.ModelAdmin):
    list_display = ("id", "camp_type")


admin.site.register(Brochure, BrochureAdmin)
